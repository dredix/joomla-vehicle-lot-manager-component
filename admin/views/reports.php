<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

	function addToolBar($addcss = '') 
	{
		$document    = & JFactory::getDocument();
		$document->addStyleSheet('templates/system/css/system.css');
		$document->addCustomTag($addcss);
		
		JToolBarHelper::title(JText::_('Report'),'reportimg');
		JToolBarHelper::custom('go_cpanel', 'cancel', 'cancel', 'Close',false);
	}

		$document->addStyleSheet($compbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
		$document->addScript($compbase."js/jquery-1.7.1.js");
		$document->addScript($compbase."js/jquery-ui-1.8.18.custom.js");
		$document->addScript($compbase."js/jquery.ui.button.js");
		$document->addScript($compbase."js/jquery.tableSort.js");
		
		$viewcss = "
<style>
	.icon-48-reportimg	{ background-image: url(".$dconfig["baseurl"]."/components/".$dconfig["comp_name"]."/images/report48.png); }
</style>";
	addToolBar($viewcss);
?>
<style>
	th{
		background-color:#77A2D4;
		color:#FFF;
	}
	td{
		text-align:left;
		border-left: 1px solid #77A2D4;
		border-bottom: 1px solid #77A2D4;
	}
	table{
		border: 1px solid #77A2D4;
	}
</style>
<script>	
	$(function() {
		$( "input:submit, a, button", ".jbtn" ).button();
	});
</script>
<?php if(!isset($_REQUEST["report"])){ ?>

<div id="form" class="ui-widget ui-widget-content ui-corner-all" style="max-width:625px; padding: 10px; margin-left:auto; margin-right:auto">
    <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    	<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>Click on one of the reports below to view it.
    </div>
    <br />

    <div class="cpanel-left">
        <div id="cpanel">
            <div class="icon-wrapper">
                <div class="icon">
                    <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=reports&report=hits">
                        <img src="<?=$compbase?>images/document48.png" alt="Files">
                        <span>Hits</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="clr"></div> 
    
    <div class="jbtn">
        <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=cpanel"><img src="<?=$compbase?>images/cpanel16.png" align="absmiddle" /> Return to Control Panel</a>
    </div>
</div>
<? }else{ ?>
	<?php if($_REQUEST["report"]=="hits") ?>
	<div id="form" class="ui-widget ui-widget-content ui-corner-all" style="max-width:500px; padding: 10px; margin-left:auto; margin-right:auto">
        <h2>Hits Reports</h2>
        
        <table width="500px" id="tbl_hits">
        	<thead>
            	<tr>
                	<th>Vehicle</th><th>Date Created</th><th>Hits</th>
                </tr>
            </thead>
            <tbody>
<?php
	$query = "SELECT i.vtitle, i.date_created, i.hits, m.list_value as vmake_name, v.list_value as vmodel_name
FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
ON i.vmake = m.prim_key
ORDER BY date_created DESC LIMIT 20";
	
	$db->setQuery($query);
	$row = $db->loadAssocList();
	for($r=0; $r < count($row); $r++)
    {
?>
				<tr>
                	<td><?php echo $row[$r]["vtitle"]; ?></td>
                    <td><?php echo $row[$r]["date_created"]; ?></td>
                    <td><?php echo $row[$r]["hits"]; ?></td>
                </tr>
<?php } ?>
            </tbody>
        </table>
    </div>    
    <script>
		$('#tbl_hits').tableSort();
		$('#tbl_hits').sortTable({
			onCol: 3,
			keepRelationships: true,
			sortType: 'numeric'
		});
		$('#tbl_hits').sortTable({
			onCol: 2,
			keepRelationships: true,
			sortType: 'numeric'
		});
	</script>
	<?php ?>
<?php } ?>

<form method="post" name="adminForm" id="adminForm">
            <input type="hidden" name="task" value="" />
</form>