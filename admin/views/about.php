<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

	function addToolBar($addcss = '') 
	{
		$document    = & JFactory::getDocument();
		$document->addStyleSheet('templates/system/css/system.css');
		$document->addCustomTag($addcss);
		
		JToolBarHelper::title(JText::_('About'),'aboutimg');
	}
	
	JToolBarHelper::custom('go_cpanel', 'cancel', 'cancel', 'Close',false);

		$document->addStyleSheet($compbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
		$document->addScript($compbase."js/jquery-1.7.1.js");
		$document->addScript($compbase."js/jquery-ui-1.8.18.custom.js");
		$document->addScript($compbase."js/jquery.ui.button.js");
		
		$viewcss = "
<style>
	.icon-48-aboutimg	{ background-image: url(".$dconfig["baseurl"]."/components/".$dconfig["comp_name"]."/images/info48.png); }
</style>";
	addToolBar($viewcss);
?>

<script>	
	$(function() {
		$( "input:submit, a, button", ".jbtn" ).button();
	});
</script>

<div id="form" class="ui-widget ui-widget-content ui-corner-all" style="padding: 10px;">
	<h3>Help</h3>
    
    <a href="http://www.dredix.net" target="_blank">Dredix website</a><br />
    <a href="#">Vehicle Lot Manager Manual</a><br />
    <a href="#">Dredix Support Forum</a><br />
    <a href="#">Software Information Page</a>
    
    <br />
	<h3>Version</h3>
    <?php echo $dconfig["ver"]; ?>
    
    <br />
  <h3>Copyright</h3>
&copy; 2012 <a href="www.dredix.net/contact-me" title="Click here to contact me" target="_blank">André Dixon</a></div>

<br />

<div id="form" class="ui-widget ui-widget-content ui-corner-all" style="padding: 10px;">
	<h3>More</h3>
    <p>
    Check out my website (<a href="http://www.dredix.net" target="_blank">dredix.net</a>) for other services and software I have to offer.<br />
	<br />
	Also, be sure to like this software by clicking the facebook icon.
    </p>
    <p>
    	As part of the Creative Commons License, I am required to give credit to the author of some of these images.
        Some of these images can be found in the URLs below:
        <ul>
        	<li><a href="http://www.iconfinder.com" target="_blank">http://www.iconfinder.com</a></li>
        	<li><a href="http://www.userinterfaceicons.com" target="_blank">http://www.userinterfaceicons.com</a></li>
        	<li><a href="http://pc.de/ico/" target="_blank">http://pc.de/ico/</a></li>
        </ul>
      
    </p>
</div>

<br />

<div id="form" class="ui-widget ui-widget-content ui-corner-all" style="padding: 10px;">
	<center>
		<script type="text/javascript"><!--
        google_ad_client = "ca-pub-3142470589068723";
        /* Long - Landscape */
        google_ad_slot = "0980579859";
        google_ad_width = 728;
        google_ad_height = 90;
        //-->
        </script>
        <script type="text/javascript"
        src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
    </center>
</div>

<br />

<div class="jbtn">
    <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=cpanel"><img src="<?=$compbase?>images/cpanel16.png" align="absmiddle" /> Return to Control Panel</a>
</div>
<form method="post" name="adminForm" id="adminForm">
            <input type="hidden" name="task" value="" />
</form>


