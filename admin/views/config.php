<?php
function addToolBar($addcss = '') 
{
	$document    = & JFactory::getDocument();
	//$document->addStyleSheet('administrator/templates/system/css/system.css');
	$document->addCustomTag($addcss);
	JToolBarHelper::title(JText::_('Configuration'),'configimg');
	JToolBarHelper::custom('go_cpanel', 'cancel', 'cancel', 'Close',false);
	
}
	$viewcss = "
<style>
	.icon-48-configimg	{ background-image: url(".$dconfig["baseurl"]."/components/".$dconfig["comp_name"]."/images/config48.png); }
</style>";
	addToolBar($viewcss);
	
	//Saving configuration if configuration data is found in the Request array
	if(isset($_REQUEST["config_form"])){
		foreach($_REQUEST["soption"] as $fkey=>$fvalue){
			$query = "UPDATE #__vlm_config h SET svalue = ".$db->Quote($db->Escape($fvalue))." WHERE soption = ".$db->Quote($db->Escape($fkey));
			$db->setQuery($query);
			$db->query();
		}
		$app->enqueueMessage( 'Featured status was updated' );
	}
	

	//Adding Javascript
	$document->addScript($compbase."js/jquery-1.7.1.js");
	$document->addScript($compbase."js/jquery.ui.core.js");
	$document->addScript($compbase."js/external/jquery.cookie.js");
	$document->addScript($compbase."js/jquery.ui.widget.js");
	$document->addScript($compbase."js/jquery.ui.button.js");
	$document->addScript($compbase."js/jquery.ui.tabs.js");
	$document->addScript($compbase."js/main.js");
	
	//Adding CSS
	$document->addStyleSheet($compbase."css/main.css");
	$document->addStyleSheet($compbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
	
?>

<jdoc:include type="message" />

<div class="content">        
    <div id="form" class="ui-widget ui-widget-content ui-corner-all" style="padding:10px">
    
<?php
	$query = "SELECT DISTINCT category FROM #__vlm_config";
	
	$db->setQuery($query);
	$tab = $db->loadAssocList();
	//print_r($row);
?>
	<script>
	$(function() {
		$( "#tabs" ).tabs({
			cookie: {
				// store cookie for a day, without, it would be a session cookie
				expires: 1
			}
		});
		$( "input:submit, a, button", ".jbtn" ).button();
	});
	</script>
    <style>
		#config_form dt{
			width: 200px;
		}
		#config_form input, #config_form select{
			background: none repeat scroll 0 0 #F7F7F7;
			border: 1px solid #D8D8D8;
			border-radius: 6px 6px 6px 6px;
			box-shadow: 0 2px 2px rgba(0, 0, 0, 0.07) inset;
			margin-bottom: 4px;
			margin-top: 4px;
			padding: 4px;
			width: 250px;
		}
	</style>
    	<form method="post" name="configform" id="configform">
        	<input type="hidden" name="config_form" value="save" />
        <div id="tabs">
            <ul>
            	<?php for($t = 0; $t < count($tab); $t++){ ?>
                <li><a href="#tabs-<?=$t?>"><?=$tab[$t]["category"]?></a></li>
                <?php } ?>
            </ul>
			<?php for($t = 0; $t < count($tab); $t++){ ?>
            <div id="tabs-<?=$t?>">
            	<?php
					$query = "SELECT * FROM #__vlm_config WHERE hidden = 0 AND category = ". $db->Quote($db->Escape($tab[$t]["category"]))." ORDER BY sort_order";
					$db->setQuery($query);
					$field = $db->loadAssocList();
				?>
                <dl id="config_form">
                	<?php for($f = 0; $f < count($field); $f++){ 
						if($field[$f]["title"] != ""){$fieldname = $field[$f]["title"];}
						else{$fieldname = $field[$f]["soption"];}
					?>
                    <dt title="<?=$field[$f]["description"]?>"><?=$fieldname?></dt>
                    <dd title="<?=$field[$f]["description"]?>"s>
                    	<?php
							if($field[$f]["fieldtype"] == "text"){
							?>
						<input type="text" name="soption[<?=$field[$f]["soption"]?>]" id="<?=$field[$f]["soption"]?>" value="<?=$field[$f]["svalue"]?>" class="" />
							<?php
							}
							elseif($field[$f]["fieldtype"] == "select"){
							?>
                        <select name="soption[<?=$field[$f]["soption"]?>]" id="<?=$field[$f]["soption"]?>" class="" >
                        <?php
							$foptions = explode(";",$field[$f]["fieldoptions"]);
							for($o = 0; $o < count($foptions); $o++){
								if($field[$f]["svalue"] == $foptions[$o]){echo "\n\t\t<option value=\"".$foptions[$o]."\" selected=\"selected\">".$foptions[$o]."</option>";}
								else{echo "\n\t\t<option value=\"".$foptions[$o]."\">".$foptions[$o]."</option>";}
							}
						?>
                        </select>
							<?php
							}
							if($field[$f]["fieldtype"] == "yesno"){
							?>
                        <select name="soption[<?=$field[$f]["soption"]?>]" id="<?=$field[$f]["soption"]?>" class="" >
                        <?php
							if($field[$f]["svalue"] == 1){echo "<option value='1' selected=\"selected\">Yes</option><option value='0'>No</option>";}
							else{echo "<option value='1'>Yes</option><option value='0' selected=\"selected\">No</option>";}	
						?>
                        </select>
							<?php
							}							
						?>
                    </dd>
                    <?php } ?>
                </dl>
            </div>
            <?php } ?>
        </div>
        <br />
    	<div class="jbtn">
            <a href="javascript: document.forms['configform'].submit()"><img src="<?=$compbase?>images/save16.png" align="absmiddle" /> Save Configuration</a>
            <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=cpanel"><img src="<?=$compbase?>images/cpanel16.png" align="absmiddle" /> Return to Control Panel</a>
        </div>
        <br />
    	</form>
    </div>
</div>

<form method="post" name="adminForm" id="adminForm">
            <input type="hidden" name="task" value="" />
</form>