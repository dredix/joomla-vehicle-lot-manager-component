<?php
function addToolBar($addcss = '') 
{
	$document    = & JFactory::getDocument();
	//$document->addStyleSheet('administrator/templates/system/css/system.css');
	$document->addCustomTag($addcss);
	JToolBarHelper::title(JText::_('Vehicle Lot Manager Control Panel'),'cpanelimg');
	
}

if (!function_exists("getdbconfig")){
	function getdbconfig()
	{
		$db =& JFactory::getDBO();
		$db->setQuery("SELECT soption, svalue FROM #__vlm_config");
		$row = $db->loadAssocList();
		
		for($r = 0; $r < count($row); $r++){
			$outarr[$row[$r]["soption"]] = $row[$r]["svalue"];
		}
		return $outarr;
	}
}
$vconfig = getdbconfig();

	$viewcss = "
<style>
	.icon-48-cpanelimg	{ background-image: url(".$dconfig["baseurl"]."/components/".$dconfig["comp_name"]."/images/cpanel48.png); }
</style>";
	addToolBar($viewcss);
	
	
	$document->addScript($compbase."js/jquery-1.7.1.js");
	
	$document->addStyleSheet($compbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
?>
<style>
	#someinfo th{
		background-color: #DCDCDC;
	}
	#someinfo td{
		padding:5px
	}
	
	div.btn{
		padding:10px;
		margin:10px;
		border:1px solid #000;
		width:80px;
		text-align:center;
		text-decoration:none;
		float:left;
		border-radius: 7px;
		opacity: .7;
	}
	div.btn:hover{
		background:#A6D7E8;
		border:1px solid #3476DD;
		opacity: 1;
	}
</style>

<div id="form" class="ui-widget ui-widget-content ui-corner-all" style="padding:10px">

<div class="cpanel-left">
	<div id="cpanel">
		<div class="icon-wrapper">
            <div class="icon">
                <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=vehicles">
                    <img src="<?=$compbase?>images/vehicle48.png" alt="Files">
                    <span>Vehicles</span>
                </a>
            </div>
        </div>
        
        <div class="icon-wrapper">
            <div class="icon">
                <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=addvehicles">
                    <img src="<?=$compbase?>images/addv48.png" alt="Files">
                    <span>Add Vehicles</span>
                </a>
            </div>
        </div>
        
        <div class="icon-wrapper">
            <div class="icon">
                <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=setup">
                    <img src="<?=$compbase?>images/setup48.png" alt="Files">
                    <span>Setup</span>
                </a>
            </div>
        </div>        
        
        <div class="icon-wrapper">
            <div class="icon">
                <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=config">
                    <img src="<?=$compbase?>images/config48.png" alt="Files">
                    <span>Configuration</span>
                </a>
            </div>
        </div>  
        
        <div class="icon-wrapper">
            <div class="icon">
                <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=reports">
                    <img src="<?=$compbase?>images/report48.png" alt="Files">
                    <span>Reports</span>
                </a>
            </div>
        </div> 
        
        <div class="icon-wrapper">
            <div class="icon">
                <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=about">
                    <img src="<?=$compbase?>images/info48.png" alt="Files">
                    <span>About</span>
                </a>
            </div>
        </div>                 
        			
		<div style="clear:both">&nbsp;</div>
		<p>&nbsp;</p>
		<div style="text-align:center;padding:0;margin:0;border:0">

		</div>
	</div>
</div>


	
<div class="clr"></div>

<table width="400" border="0" id="someinfo">
    <tr>
        <th>Support:</th>
        <td>
        	Email: <a href="mailto:dredix@dredix.net">dredix@dredix.net</a><br />
			Phone: 876-420-4881<br />
			Website: <a href="http://www.dredix.net" target="_blank">http://www.dredix.net</a>
        </td>
    </tr>
    <tr>
        <th>Licence:</th>
        <td>© <?=date("Y")?>
        <a href="www.dredix.net/contact-me" target="_blank">André Dixon</a></td>
    </tr>
    <tr>
        <th>Version:</th>
        <td><?=$vconfig["version"]?></td>
    </tr>
</table>


</div>


