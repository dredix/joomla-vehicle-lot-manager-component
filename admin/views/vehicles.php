<?php
defined('_JEXEC') or die('Restricted access');

include("proc_vechiles.php");


	
	//Adding Javascript
	$document->addScript($compbase."js/jquery-1.7.1.js");
	$document->addScript($compbase."js/jquery-ui-1.8.18.custom.js");
	$document->addScript($compbase."js/jquery.ui.dialog.js");
	$document->addScript($compbase."js/jquery.ui.button.js");
	$document->addScript($compbase."js/jquery.dataTables.min.js");
	$document->addScript($compbase."js/main.js");
	
	//Adding CSS
	$document->addStyleSheet($compbase."css/jquery.dataTables.css");
	$document->addStyleSheet($compbase."css/main.css");
	$document->addStyleSheet($compbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");

	//addToolBar();
		$viewcss = "
<style>
</style>
";
	addToolBar($viewcss);
?>


<?php
	$query = "SELECT i.*, m.list_value as vmake_name, v.list_value as vmodel_name
FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
ON i.vmake = m.prim_key;";
	
	$db->setQuery($query);
	$row = $db->loadAssocList();
	//print_r($row);
?>

	<jdoc:include type="message" />

	<div id="vsearch" class="hide flexigrid">
        <div class="mDiv">
        	<div class="ftitle">Vehicle Search</div>
        </div>
        <div class="content">
			<table cellpadding="0" cellspacing="0" border="0" width="600px">
				<thead>
				<tr>
					<th>Target</th>
					<th>Filter text</th>
					<th>Treat as regex</th>
					<th>Use smart filter</th>
				</tr>
				</thead>
				<tbody>
					<tr id="filter_global">
						<td align="center">Global filtering</td>
						<td align="center"><input type="text"     name="global_filter" id="global_filter"></td>
						<td align="center"><input type="checkbox" name="global_regex"  id="global_regex" ></td>
						<td align="center"><input type="checkbox" name="global_smart"  id="global_smart"  checked></td>
					</tr>
					<tr id="filter_col1">
						<td align="center">Asset #</td>
						<td align="center"><input type="text"     name="col1_filter" id="col1_filter"></td>
						<td align="center"><input type="checkbox" name="col1_regex"  id="col1_regex"></td>
						<td align="center"><input type="checkbox" name="col1_smart"  id="col1_smart" checked></td>
					</tr>
					<tr id="filter_col2">
						<td align="center">Title</td>
						<td align="center"><input type="text"     name="col2_filter" id="col2_filter"></td>
						<td align="center"><input type="checkbox" name="col2_regex"  id="col2_regex"></td>
						<td align="center"><input type="checkbox" name="col2_smart"  id="col2_smart" checked></td>
					</tr>
					<tr id="filter_col3">
						<td align="center">Make</td>
						<td align="center"><input type="text"     name="col3_filter" id="col3_filter"></td>
						<td align="center"><input type="checkbox" name="col3_regex"  id="col3_regex"></td>
						<td align="center"><input type="checkbox" name="col3_smart"  id="col3_smart" checked></td>
					</tr>
					<tr id="filter_col4">
						<td align="center">Model</td>
						<td align="center"><input type="text"     name="col4_filter" id="col4_filter"></td>
						<td align="center"><input type="checkbox" name="col4_regex"  id="col4_regex"></td>
						<td align="center"><input type="checkbox" name="col4_smart"  id="col4_smart" checked></td>
					</tr>
					<tr id="filter_col5">
						<td align="center">Year</td>
						<td align="center"><input type="text"     name="col5_filter" id="col5_filter"></td>
						<td align="center"><input type="checkbox" name="col5_regex"  id="col5_regex"></td>
						<td align="center"><input type="checkbox" name="col5_smart"  id="col5_smart" checked></td>
					</tr>
				</tbody>
			</table>
        </div>
    </div>
    
    <div class="action_btns jbtn">
    	<a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=addvehicles" title="Add a vehicle">
        	<img src="<?php echo $compbase; ?>images/add16.png" alt="Search" align="absmiddle" /> Add Vehicle
        </a>
    	<a onclick="showsearch(); return false;" title="Toggle vehicle search options">
        	<img src="<?php echo $compbase; ?>images/magnifier16.png" alt="Search" align="absmiddle" /> Search
        </a>
        <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=cpanel"><img src="<?=$compbase?>images/cpanel16.png" align="absmiddle" /> Return to Control Panel</a>
    </div>
    
    <table id="table_vehicles" class="display">
        <thead>
            <tr>
                <th>Asset #</th>
                <th>Title</th>
                <th>Make</th>
                <th>Model</th>
                <th width="40px">Year</th>
                <th>Price</th>
                <th width="100px">Created</th>
                <th width="25px">Hits</th>
                <th width="90px"></th>
            </tr>
        </thead>
        <tbody>
        <?php
            for($r=0; $r < count($row); $r++)
            {
        ?>
            <tr>
                <td><?php echo $row[$r]["asset_code"]; ?></td>
                <td>
                	<a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=editvehicles&vehicle_id=<?php echo $row[$r]["vehicle_id"]; ?>">
					<strong><?php echo $row[$r]["vtitle"]; ?></strong>
                    </a>
                </td>
                <td><?php echo $row[$r]["vmake_name"]; ?></td>
                <td><?php echo $row[$r]["vmodel_name"]; ?></td>
                <td><?php echo $row[$r]["vyear"]; ?></td>
                <td><?php echo $dconfig["price_sym"].$row[$r]["price"]." ".$row[$r]["price_unit"]; ?></td>
                <td><?php echo $row[$r]["date_created"]; ?></td>
                <td><?php echo $row[$r]["hits"]; ?></td>
                <td>
				<?php 
					//Setting published icon
					
					if($row[$r]["published"]=="1"){
						echo '<img src="'.$compbase.'images/tick.png" alt="Published" onclick="chg_status('.$row[$r]["vehicle_id"].',\''.$row[$r]["vtitle"].'\',\'published\',0)" /> ';
					}else{
						echo '<img src="'.$compbase.'images/publish_x.png" alt="Not Published" onclick="chg_status('.$row[$r]["vehicle_id"].',\''.$row[$r]["vtitle"].'\',\'published\',1)"  /> ';
					}
					
					//Setting featured icon
					if($row[$r]["featured"]=="1"){
						echo '<img src="'.$compbase.'images/featured.png" alt="Featured" onclick="chg_status('.$row[$r]["vehicle_id"].',\''.$row[$r]["vtitle"].'\',\'featured\',0)"/>';
					}else{
						echo '<img src="'.$compbase.'images/disabled.png" alt="Not Featured" onclick="chg_status('.$row[$r]["vehicle_id"].',\''.$row[$r]["vtitle"].'\',\'featured\',1)"/>';
					}					
					
				?>
                    <a href="#"><img src="<?php echo $compbase."images/" ?>info16.png" alt="Info" /></a> 
                    <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=editvehicles&vehicle_id=<?php echo $row[$r]["vehicle_id"]; ?>">
                    	<img src="<?php echo $compbase."images/" ?>edit16.png" alt="Edit" title="Edit this record" />
                    </a>
                    <img onclick="delete_record(<?php echo $row[$r]["vehicle_id"]; ?>,'<?php echo htmlentities($row[$r]["vtitle"]); ?>');" src="<?php echo $compbase."images/" ?>remove16.png" alt="X" title="Delete this record" />
                </td>
            </tr>
        <?php
            }
        ?>
        </tbody>
    </table>


<script type="text/javascript">
	function delete_record(rid,dtitle){
		$('#delid').val(rid);	//
		$('#del_title').html(dtitle);	//
		$( "#del_dialog" ).dialog( "open" );
	}
	
	function chg_status(sid,dtitle, statusf, statusv){
		$('#sid').val(sid);	//
		$('#status_title').html(dtitle);	//
		$('#statusf').val(statusf);	//
		$('#statusv').val(statusv);	//
		$( "#status_dialog" ).dialog( "open" );
	}
	
	
	function doadd(){
		alert("Add action here");
	}
	
	function showsearch(){
		toggleDiv('vsearch');
	}	
	
	//For Data table
	function fnFilterGlobal ()
	{
		$('#table_vehicles').dataTable().fnFilter( 
			$("#global_filter").val(),
			null, 
			$("#global_regex")[0].checked, 
			$("#global_smart")[0].checked
		);
	}
	
	function fnFilterColumn ( i )
	{
		$('#table_vehicles').dataTable().fnFilter( 
			$("#col"+(i+1)+"_filter").val(),
			i, 
			$("#col"+(i+1)+"_regex")[0].checked, 
			$("#col"+(i+1)+"_smart")[0].checked
		);
	}
	
	
	$(document).ready(function() {
		$('#table_vehicles').dataTable({
			"bJQueryUI": false,
			"bStateSave": true,
        	"sPaginationType": "full_numbers"
    	});
		
		$("#global_filter").keyup( function() { fnFilterGlobal(); } );
		
		$("#col1_filter").keyup( function() { fnFilterColumn( 0 ); } );
		$("#col1_regex").click(  function() { fnFilterColumn( 0 ); } );
		$("#col1_smart").click(  function() { fnFilterColumn( 0 ); } );
		
		$("#col2_filter").keyup( function() { fnFilterColumn( 1 ); } );
		$("#col2_regex").click(  function() { fnFilterColumn( 1 ); } );
		$("#col2_smart").click(  function() { fnFilterColumn( 1 ); } );
		
		$("#col3_filter").keyup( function() { fnFilterColumn( 2 ); } );
		$("#col3_regex").click(  function() { fnFilterColumn( 2 ); } );
		$("#col3_smart").click(  function() { fnFilterColumn( 2 ); } );
		
		$("#col4_filter").keyup( function() { fnFilterColumn( 3 ); } );
		$("#col4_regex").click(  function() { fnFilterColumn( 3 ); } );
		$("#col4_smart").click(  function() { fnFilterColumn( 3 ); } );
		
		$("#col5_filter").keyup( function() { fnFilterColumn( 4 ); } );
		$("#col5_regex").click(  function() { fnFilterColumn( 4 ); } );
		$("#col5_smart").click(  function() { fnFilterColumn( 4 ); } );
	} );
	
	$(function() {
		$( "input:submit, a, button", ".jbtn" ).button();
		//$( "a", ".demo" ).click(function() { return false; });
	});
</script>

	<script>
	// increase the default animation speed to exaggerate the effect
	$.fx.speeds._default = 1000;
	$(function() {
		$( "#del_dialog, #status_dialog" ).dialog({
			autoOpen: false,
			show: "blind",
			hide: "explode",
			height: 120,
			modal: true
		});
	});
	</script>




<div id="del_dialog" title="Confirm Delete">
	<center>
        <form name="frm_delete" action="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=vehicles" method="post">
        	Are you sure you want to delete?<br />
            Title: <span id="del_title" style="font-weight:bold"></span>
        	<br /><br />
            <input type="hidden" value="0" name="delid" id="delid" />
            <input type="hidden" value="del_record" name="action" id="action" />
            <input type="hidden" value="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=vehicles" name="returnurl" id="returnurl" />

            <div class="jbtn"><input type="submit" value="Click here to confirm"  /></div>
        </form>
    </center>
</div>

<div id="status_dialog" title="Status Change">
	<center>
        <form name="frm_status" method="post">
        	Are you sure you want to change the status for <span id="status_title" style="font-weight:bold"></span>?
        	<br /><br />
            <input type="hidden" value="0" name="sid" id="sid" />
            <input type="hidden" value="chg_status" name="action" id="action" />
            <input type="hidden" value="" name="statusf" id="statusf" />
            <input type="hidden" value="" name="statusv" id="statusv" />

            <div class="jbtn"><input type="submit" value="Click here to confirm"  /></div>
        </form>
    </center>
</div>

<form method="post" name="adminForm" id="adminForm">
            <input type="hidden" name="task" value="" />
</form>