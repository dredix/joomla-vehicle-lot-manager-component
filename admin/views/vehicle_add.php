<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

	function addToolBar($addcss = '') 
	{
		$document    = & JFactory::getDocument();
		$document->addStyleSheet('templates/system/css/system.css');
		$document->addCustomTag($addcss);
		
		JToolBarHelper::title(JText::_('Add Vehicle'),'addvehicleimg');
		
		//JToolBarHelper::save();
		//JToolBarHelper::apply();
		JToolBarHelper::custom('go_cpanel', 'cancel', 'cancel', 'Canel',false);
	}

	include("proc_vehicle_add.php");
	
	//Adding Javascript
	$document->addScript($compbase."js/jquery-1.7.1.js");
	$document->addScript($compbase."js/jquery-ui-1.8.18.custom.js");
	$document->addScript($compbase."js/ui.multiselect.js");
	$document->addScript($compbase."js/jquery.ui.button.js");
	$document->addScript($compbase."js/jquery.ui.tabs.js");
	$document->addScript($compbase."js/main.js");
	$document->addScript($compbase."js/jquery.wysiwyg.js");
	$document->addScript($compbase."js/yav.js");
	$document->addScript($compbase."js/yav-config.js");
	
	$document->addScript($dconfig["baseurl"]."../media/system/js/core.js");
	$document->addScript($dconfig["baseurl"]."../media/system/js/modal.js");
	$document->addScript($dconfig["baseurl"]."../media/system/js/mootools-core.js");
	$document->addScript($dconfig["baseurl"]."../media/system/js/mootools-more.js");

	
	//Adding CSS
	$document->addStyleSheet($compbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
	$document->addStyleSheet($compbase."css/ui.multiselect.css");
	$document->addStyleSheet($compbase."css/jquery.wysiwyg.css");
	$document->addStyleSheet($compbase."css/main.css");
	$document->addStyleSheet($dconfig["baseurl"]."../media/system/css/modal.css");
	

	//addToolBar();
		$viewcss = "
<style>

</style>";
	addToolBar($viewcss);
?>

<jdoc:include type="message" />
<?php
	//JHTMLBehavior::formvalidation(); 
	JHtml::_('behavior.formvalidation');
?>
<script>
	$(function() {
		$( "#form" ).tabs();
	});
	
	$(function() {
		$( "input:submit, a, button", ".jbtn" ).button();
		//$( "a", ".demo" ).click(function() { return false; });
	});
	
	$(function(){
		$(".multiselect").multiselect();
	});
	
  $(function(){
      $('#contact_info').wysiwyg();
  });



    var rules=new Array();
    rules[0]='vtitle:Title|required';
    rules[1]='vmake:Make|required';
	rules[2]='vmodel:Model|required';
	rules[3]='vyear:Year|required';
	rules[4]='vyear:Year|required';
	rules[4]='vyear:Year|numrange|1900-<?php echo date("Y") + 2; ?>';
	rules[5]='doors:Doors|required';
	rules[6]='vinfo[description]:Description|required';
	
	rules[7]='colour_ext:Colour Ext|required';
	
	function formvalid(){
		if(!yav.performCheck('adminForm', rules, 'classic')){
			//return false;
		}else{
			document.forms['adminForm'].submit()
			//return true;
		}
		
	}
</script>
<div class="content">
	<?php
	//$config =& JFactory::getConfig();
	//echo 'DB Table prefix: ' . $config->getValue( 'config.dbprefix' );
	//return yav.performCheck('adminForm', rules, 'classic')
	?>
    
    <form id="adminForm" name="adminForm" action="" method="post" class="form-validate" onsubmit="return false;">
    	<div class="jbtn">
            <a href="javascript: formvalid();"><img src="<?=$compbase?>images/save16.png" align="absmiddle" /> Save Information</a>
            <a href="javascript: $('#save_close').val('1'); formvalid()"><img src="<?=$compbase?>images/save16.png" align="absmiddle" /> Save & Close</a>
            <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=vehicles"><img src="<?=$compbase?>images/back_arrow16.png" align="absmiddle" /> Return to list</a>
        </div>
        
    	<div id="form" class="ui-widget ui-widget-content ui-corner-all">
    
            <ul>
                <li><a href="#tabs-1">General Details</a></li>
                <li><a href="#tabs-2">Images</a></li>
            </ul>
   
        	<div id="tabs-1">
                <?php if(isset($_REQUEST["vehicle_id"])){ ?><input type="hidden" name="vehicle_id" id="vehicle_id" value="<?php echo $row["vehicle_id"]; ?>" /><?php } ?>
                <input type="hidden" name="do_action" value="save" />
                <input type="hidden" name="save_close" id="save_close" value="0" />
                <input type="hidden" name="task" id="task" value=""/>
                <table border="0" cellspacing="2" cellpadding="0" class="dtable">
                    <tr>
                        <td><label for="vtitle">Title</label></td>
                        <td><input type="text" name="vinfo[vtitle]" id="vtitle" class="required i_long" value="<?php echo $row["vtitle"]; ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="description">Description</label></td>
                        <td>
                            <?php
                                $editor =& JFactory::getEditor();
                                echo $editor->display('vinfo[description]', $row["description"], '400', '400', '40', '20', false);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="asset_code">Asset Code</label></td>
                        <td><span class="editlinktip hasTip" title="::If you have an organization code used for this vehicle, enter it here." ><input type="text" name="vinfo[asset_code]" class="i_med" value="<?php echo $row["asset_code"]; ?>" /></span></td>
                    </tr>
                    <tr>
                        <td><label>Vin Number</label></td>
                        <td><input type="text" name="vinfo[vin_no]" class="i_med" value="<?php echo $row["vin_no"]; ?>" maxlength="50" /></td>
                    </tr>
                    <tr>
                        <td><label for="category_id">Category</label></td>
                        <td>
                            <select name="vinfo[category_id]" class="i_med" id="category_id">
                                <option value="">Category ... </option>
                                <?php echo getdroplist("Category", $row["category_id"]); ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="vmake">Make</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::What is the name of the company that manufactured this vehicle?" >
                            <select name="vinfo[vmake]" id="vmake" class="required i_med">
                                <option value="">Select make ... </option>
                                <?php echo getdroplist("Make", $row["vmake"]); ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="vmodel">Model</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::What is the model of this vehicle?" >
                            <select name="vinfo[vmodel]" id="vmodel" class="required i_med">
                                <option value="">Select model ... </option>
                                <?php echo getdroplist("Model", $row["vmodel"]); ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="price">Price</label></td>
                        <td>
                        <span class="editlinktip hasTip" title="Price::What price should this vehicle be sold for?" >
                        	<input type="text" name="vinfo[price]" class="i_small" value="<?php echo $row["price"]; ?>" />
                        </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="price">Consession Price</label></td>
                        <td>
                        <span class="editlinktip hasTip" title="Price::What price could this vehicle be sold for if a duty consession is applied?" >
                        	<input type="text" name="vinfo[consession_price]" class="i_small" value="<?php echo $row["consession_price"]; ?>" />
                        </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="price_unit">Price Unit</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::What currency should the vehicle be quoted in?" >
                            <select name="vinfo[price_unit]" class="i_med">
                                <option value="">Price Unit ... </option>
                                <?php echo getdroplist("Price Unit", $row["price_unit"], false); ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="price_type">Price Type</label></td>
                        <td>
                            <select name="vinfo[price_type]" class="i_med">
                                <option value="">Price Type ... </option>
                                <?php echo getdroplist("Price Type", $row["price_type"], false); ?>
                            </select>                        
                        </td>
                    </tr>
                    <tr>
                        <td><label for="vtype">Type</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title=":What family of vehicles does this vehicle belong to?" >
							<select name="vinfo[vtype]" class="i_med">
                                <option value="">Type ... </option>
                                <?php echo getdroplist("Type", $row["vtype"]); ?>
                            </select>  
                        </td>
                    </tr>
                    <tr>
                        <td><label for="vlocation">Location</label></td>
                        <td>
                            <span class="editlinktip hasTip" title="::What is the vehicle current location or where will it be stored?" >
                            <input type="text" name="vinfo[vlocation]" class="i_long" value="<?php echo $row["vlocation"]; ?>" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="contact_info">Contact Info</label></td>
                        <td><textarea name="vinfo[contact_info]" id="contact_info" rows="15" cols="50"><?php echo $row["contact_info"]; ?></textarea> </td>
                    </tr>
                    <tr>
                        <td><label for="vyear">Year</label></td>
                        <td><span class="editlinktip hasTip" title="::What year was the vehicle manufactured?" ><input type="text" name="vinfo[vyear]" id="vyear" class="required i_small" value="<?php echo $row["vyear"]; ?>" /></span></td>
                    </tr>
                    <tr>
                        <td><label for="vcondition">Condition</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::What is the overall condition of the vehicle?" >
                            <select name="vinfo[vcondition]" class="required i_med" id="vcondition">
                                <option value="">Condition ... </option>
                                <?php echo getdroplist("Condition", $row["vcondition"], false); ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="mileage">Mileage</label></td>
                        <td>
                            <span class="editlinktip hasTip" title="::What is the current mileage on the vehicle?" >
                            <input type="text" name="vinfo[mileage]" class="i_small" value="<?php echo $row["mileage"]; ?>" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="mileage_unit">Mileage Unit</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::What unit of measurement in the mileage calculated in?" >
                            <select name="vinfo[mileage_unit]" class="i_med">
                                <option value="">Select Unit</option>
                                <option value="Kmph" <?php if($row["mileage_unit"] == "Kmph"){echo "selected=\"selected\"";} ?>>Kmph</option>
                                <option value="Mph" <?php if($row["mileage_unit"] == "Mph"){echo "selected=\"selected\"";} ?>>Mph</option>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>MPG</label></td>
                        <td>
                            <span class="editlinktip hasTip" title="::Estimated miles per gallon?" >
                            <input type="text" name="vinfo[mpg]" class="i_med" value="<?php echo $row["mpg"]; ?>" maxlength="45" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="listing_status">Listing Status</label></td>
                        <td>
                            <select name="vinfo[listing_status]" class="i_med">
                                <option value="">Listing Status ... </option>
                                <?php echo getdroplist("Listing Status", $row["listing_status"]); ?>
                            </select>                        
                        </td>
                    </tr>
                    <tr>
                        <td><label for="transmission">Transmission</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::What type of transmission does the vehicle have?" >
                            <select name="vinfo[transmission]" class="i_med">
                                <option value="">Transmission ... </option>
                                <?php echo getdroplist("Transmission", $row["transmission"], false); ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="colour_ext">Colour Ext</label></td>
                        <td>
                            <span class="editlinktip hasTip" title="Colour::What is the exterior colour of the vehicle?" >
                            <input type="text" name="vinfo[colour_ext]" id="colour_ext" class="i_med" value="<?php echo $row["colour_ext"]; ?>" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="colour_int">Colour Int</label></td>
                        <td>
                            <span class="editlinktip hasTip" title="Colour::What is the interior colour (seat, dashboard, etc..)?" >
                            <input type="text" name="vinfo[colour_int]" class="i_med" value="<?php echo $row["colour_int"]; ?>" />
                            </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td><label for="doors">Doors</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::How many doors does the vehicle have?<br />(Note: SUV back doors usually included in door count)" >
                            <select name="vinfo[doors]" class="required i_small" id="doors">
                                <option value="">Doors ... </option>
                                <?php echo getdroplist("Doors", $row["doors"], false); ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="engine">Engine</label></td>
                        <td>
                        <span class="editlinktip hasTip" title="::What type or power engine is in the vehicle?" >
                        <input type="text" name="vinfo[engine]" class="i_med" value="<?php echo $row["engine"]; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="fuel_type">Fuel Type</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::What type of fuel does this vehicle use?" >
                            <select name="vinfo[fuel_type]" class="i_small">
                                <option value="">Fuel Type ... </option>
                                <?php echo getdroplist("Fuel Type", $row["fuel_type"], false); ?>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="features">Features</label></td>
                        <td>
                            <select name="vinfo[features][]" id="features"  multiple="multiple" class="multiselect" style="width:600px; height:250px">
                                <?php 
									$features = explode(";",$row["features"]);
									echo getmultilist("Features", $features, false); 
								?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="published">Published</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::Should this vehicle be shown on the website?" >
                            <select name="vinfo[published]" class="i_small">
                                <option value="">Select ...</option>
                                <option value="0" <?php if($row["published"] == 0){echo "selected=\"selected\"";} ?>>No</option>
                                <option value="1" <?php if($row["published"] == 1){echo "selected=\"selected\"";} ?>>Yes</option>
                            </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="featured">Featured</label></td>
                        <td>
                        	<span class="editlinktip hasTip" title="::Should this vehicle be featured on the website?" >
                            <select name="vinfo[featured]" class="i_small">
                                <option value="">Select ...</option>
                                <option value="0" <?php if($row["featured"] == 0){echo "selected=\"selected\"";} ?>>No</option>
                                <option value="1" <?php if($row["featured"] == 1){echo "selected=\"selected\"";} ?>>Yes</option>
                            </select>
                            </span>
                        </td>
                    </tr>                    
                    <tr>
                        <td><label for="hits">Hits</label></td>
                        <td>
                            <span class="editlinktip hasTip" title="View/Hit Count::This shows how many times this vehicle has been viewed on the website?" >
                            <input type="text" name="hits" class="i_small" readonly="readonly" value="<?php echo $row["hits"]; ?>" />
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            
            <div id="tabs-2">

    
  <script type="text/javascript">
window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});
		
	function jInsertFieldValue(value, id) {
		var old_id = document.id(id).value;
		if (old_id != id) {
			var elem = document.id(id);
			elem.value = value;
			elem.fireEvent("change");
		}
	}
	
	function ipreview(img){
		if(img != ""){document.getElementById("ipreview").innerHTML = "<img src=\"<?=$dconfig["baseurl"]."../"?>"+img+"\" class=\"addvipreview_large\" />";}
		else{$("#ipreview").html("");}
	}
	
	function addimg(){
		var imgcnt = parseFloat($("#imgcnt").val());
		document.getElementById("moreimg").innerHTML += "\n<span id='simg"+imgcnt+"'><li> <input type=\"text\" name=\"vinfo[vimage][]\" id=\"vimage"+imgcnt+"\" value=\"\" readonly=\"readonly\" onclick=\"ipreview(this.value);\" /> <a class=\"modal\" title=\"Select\" href=\"index.php?option=com_media&amp;view=images&amp;tmpl=component&amp;asset=com_menus&amp;author=&amp;fieldid=vimage"+imgcnt+"&amp;folder=\" rel=\"{handler: 'iframe', size: {x: 800, y: 500}}\">Select</a> <img onclick=\"removeimg("+imgcnt+")\" src=\"<?php echo $compbase."images/" ?>remove16.png\" title=\"Delete this record\" /></li></span>";
		$("#imgcnt").val(imgcnt + 1);	//Increment counter for images
	}
	
	function removeimg(x){
		$("#simg"+x).html("");
	}

</script>	
   	
    <span id="ipreview" style="float:right"></span>
        
    <ol>     
    <?php
		for ($i = 1; $i < 21; $i++){
			$vimage = explode(";",$row["vimage"]);
			if(isset($vimage[$i-1])){$imgval = $vimage[$i-1];}
			else{$imgval = "";}
	?>
          
        <li>
            <input type="text" name="vinfo[vimage][]" id="vimage<?=$i?>" value="<?=$imgval?>" class="i_long" readonly="readonly" onclick="ipreview(this.value);" />
            <a class="modal" title="Select" href="index.php?option=com_media&amp;view=images&amp;tmpl=component&amp;asset=com_menus&amp;author=&amp;fieldid=vimage<?=$i?>&amp;folder=" rel="{handler: 'iframe', size: {x: 800, y: 500}}">
            <img align="absmiddle" src="<?php echo $compbase."images/" ?>insert-image16.png" title="Select an image" alt="Select Image" />
            </a>
            
            <img align="absmiddle" onclick="$('#vimage<?=$i?>').val('')" src="<?php echo $compbase."images/" ?>remove16.png" title="Remove this image" alt="Remove Image" /> 
            <img align="absmiddle" onclick="ipreview($('#vimage<?=$i?>').val());" src="<?php echo $compbase."images/" ?>image16.png" title="Preview this image" alt="Preview" />
        </li>
        
     <?php
		}
		
		
	 ?>      
        <span id="moreimg"></span> 
    </ol>     
    
    <div class="ui-widget ui-widget-content ui-corner-all">
    <h3>Image Preview</h3>
    
    <?php
		if(count($vimage) > 0){
			for($i = 0; $i < count($vimage); $i++){
				if($vimage[$i] != ""){
					echo "<img src=\"".$dconfig["baseurl"]."../".$vimage[$i]."\" class=\"addvipreview\" title=\"".($i+1)."\" />";
				}
			}	
		}
	?>
    </div>    

    
           
            </div>

    	</div>
        <br />
        <div class="jbtn">
        	<a href="javascript: formvalid()"><img src="<?=$compbase?>images/save16.png" align="absmiddle" /> Save Information</a>
            <a href="javascript: $('#save_close').val('1'); formvalid()"><img src="<?=$compbase?>images/save16.png" align="absmiddle" /> Save & Close</a>
            <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=vehicles"><img src="<?=$compbase?>images/back_arrow16.png" align="absmiddle" /> Return to list</a>
        </div>
    </form>
    
</div>
