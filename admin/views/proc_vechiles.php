<?php
defined('_JEXEC') or die('Restricted access');

function addToolBar($addcss = '') 
{
	$document    = & JFactory::getDocument();
	$document->addStyleSheet('templates/system/css/system.css');
	$document->addCustomTag($addcss);
	
	JToolBarHelper::title(JText::_('Vehicles'),'vehicleimg');
	
	//Adding Buttons
	JToolBarHelper::custom('addvehicles', 'addvehicleimg', 'addvehicleimg', 'Add Vehicle',false);
	JToolBarHelper::custom('go_cpanel', 'cancel', 'cancel', 'Close',false);
}




if(isset($_POST["action"])){
	
	if($_POST["action"]=="del_record"){	
		//Delete a record
		//die("Deleting record: ".$_POST["delid"]);
		$query = "DELETE FROM #__vlm_vehicle WHERE vehicle_id = ".$_POST["delid"];
		$db->setQuery($query);
		$db->query();
		
		JError::raiseNotice( 100, 'The record was deleted' );	//Setting notice message
		//if($_POST["returnurl"] != ""){header('Location: '.$_POST["returnurl"]);}
	}
	
	
	if($_POST["action"]=="chg_status"){
		if($_POST["statusf"]=="published"){
			$query = "UPDATE #__vlm_vehicle SET published = ".$_POST["statusv"]." WHERE vehicle_id = ".$_POST["sid"];
			$db->setQuery($query);
			$db->query();
			$app->enqueueMessage( 'Published status was updated' );
		}
		if($_POST["statusf"]=="featured"){
			$query = "UPDATE #__vlm_vehicle SET featured = ".$_POST["statusv"]." WHERE vehicle_id = ".$_POST["sid"];
			$db->setQuery($query);
			$db->query();
			$app->enqueueMessage( 'Featured status was updated' );
		}
	}
}


?>