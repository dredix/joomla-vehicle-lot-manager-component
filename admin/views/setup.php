<?php
defined('_JEXEC') or die('Restricted access');

include("proc_setup.php");


function addToolBar($addcss = '') 
{
	$document    = & JFactory::getDocument();
	$document->addStyleSheet('administrator/templates/system/css/system.css');
	$document->addCustomTag($addcss);
	JToolBarHelper::title(JText::_('Setup'),'setupimg');
	JToolBarHelper::custom('go_cpanel', 'cancel', 'cancel', 'Close',false);
}


	//Adding Javascript
	$document->addScript($compbase."js/jquery-1.7.1.js");
	$document->addScript($compbase."js/jquery-ui-1.8.18.custom.js");
	$document->addScript($compbase."js/jquery.ui.button.js");
	$document->addScript($compbase."js/jquery.ui.dialog.js");
	$document->addScript($compbase."js/jquery.dataTables.min.js");
	$document->addScript($compbase."js/main.js");
	
	//Adding CSS
	$document->addStyleSheet($compbase."css/jquery.dataTables.css");
	$document->addStyleSheet($compbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
	$document->addStyleSheet($compbase."css/main.css");


	$viewcss = "
<style>
	.icon-48-setupimg	{ background-image: url(".$dconfig["baseurl"]."/components/".$dconfig["comp_name"]."/images/setup48.png); }
</style>";
	addToolBar($viewcss);
?>


<jdoc:include type="message" />

<script>	
	$(function() {
		$( "input:submit, a, button", ".jbtn" ).button();
	});
	
	
	$(document).ready(function() {
		$('#fdata').dataTable({
			"bJQueryUI": false,
			"bStateSave": true,
        	"sPaginationType": "full_numbers"
    	});
	});
	
	$.fx.speeds._default = 1000;
	$(function() {
		$( "#add_dialog,#edit_dialog,#del_dialog" ).dialog({
			autoOpen: false,
			show: "blind",
			hide: "explode",
			/*height: 120,*/
			modal: true
		});
	});
</script>

<?php
	if(!isset($_REQUEST["list_name"]))
	{
?>

<div id="form" class="ui-widget ui-widget-content ui-corner-all" style="max-width:625px; padding: 10px; margin-left:auto; margin-right:auto">
<div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>The buttons below allow you to modify the items in drop down list/field when creating or editing a vehicles. This allows you to customize this software to your liking.<br />
Click on one of the buttons below to get started.
</div>
<br />
	
 
    <!--<div class="jbtn"> -->
    <div id="cpanel">
    <?php
        $query = "SELECT * FROM #__vlm_droplistcat";
        $db->setQuery($query);
        $row = $db->loadAssocList();
        
        for($r=0; $r < count($row); $r++)
        {
			
			if($row[$r]["parent_name"] != ""){
				$parent = "&parent_name=".str_replace(" ","+", $row[$r]["parent_name"]);
			}
			else {$parent = "";}
    ?>
        <!--<a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=setup&list_name=<?php echo str_replace(" ","+", $row[$r]["list_name"]).$parent; ?>">
		<?php if($row[$r]["image"] != ""){ ?><img src="<?=$compbase.$row[$r]["image"]?>" align="absmiddle" alt='[]' /> <?php } ?><?php echo $row[$r]["list_name"]; ?>
        </a>  -->
		<div class="icon-wrapper">
            <div class="icon">
                <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=setup&list_name=<?php echo str_replace(" ","+", $row[$r]["list_name"]).$parent; ?>">
                    <?php if($row[$r]["image"] != ""){ ?><img src="<?=$compbase.$row[$r]["image"]?>" align="absmiddle"/> <?php }else{ ?><img src="<?=$compbase?>/images/menu_item_32.png" /> <?php } ?>
                    <span><?php echo $row[$r]["list_name"]; ?></span>
                </a>
            </div>
        </div>
    <?php
        //if((($r+1)%5) == 0){echo "<br /><br />";}
        }
    ?>
    </div>
    
    <div class="clr"></div>

    
        <div class="jbtn">
            <a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=cpanel"><img src="<?=$compbase?>images/cpanel16.png" align="absmiddle" /> Return to Control Panel</a>
        </div>
</div>
<?php
	}else{
?>

<div id="form" class="ui-widget ui-widget-content ui-corner-all" style="max-width:600px; padding: 10px; margin-left:auto; margin-right:auto">
	<h3><?php echo $_REQUEST["list_name"];?></h3>
    
    <div class="action_btns jbtn">
    	<script type="text/javascript">
			function addvalue(){
				$( "#add_dialog" ).dialog( "open" );
			}
		</script>
    
    	<a onclick="addvalue();" title="Add a new value">
        	<img src="<?php echo $compbase; ?>images/add16.png" alt="Search" align="absmiddle" /> Add
        </a>
    </div>
    
    
    <table id="fdata" class="display">
    	<thead>
            <tr>
                <th>Value</th>
		<?php
			
			if(isset($_REQUEST["parent_name"]))
			{
				echo "<th>".$_REQUEST["parent_name"]."</th>";
			}
		?>
                <th>Order</th><th></th>
            </tr>
        </thead>
        <tbody>
<?php
		if(isset($_REQUEST[""])){
        	$query = "SELECT * FROM #__vlm_droplistvalues WHERE list_category=".$db->quote($_REQUEST["list_name"])." ORDER BY ordering, list_category, list_value";
		}else{
			$query = "SELECT h.`prim_key`, h.`list_category`, h.`list_value`, h.`parent_id`, h.`ordering`, m.list_value as parent_name
FROM #__vlm_droplistvalues h LEFT JOIN #__vlm_droplistvalues m
on h.parent_id = m.prim_key
WHERE h.list_category = ".$db->quote($_REQUEST["list_name"])." ORDER BY h.ordering, h.list_category, m.list_value, h.list_value";
		}
		
		$db->setQuery($query);
        $row = $db->loadAssocList();

        for($r=0; $r < count($row); $r++)
        {
?>
    	<tr>
        	<td>
            	<a href="#">
		<?php
			echo $row[$r]["list_value"];
		?>
        		</a>
        	</td>
		<?php
			
			if(isset($_REQUEST["parent_name"]))
			{
				echo "<td>".$row[$r]["parent_name"]."</td>";
			}
		?>
            
            
            <td>
            	<?php echo $row[$r]["ordering"]; ?>
            </td>
            <td>
            	<?php 
					if(isset($_REQUEST["parent_name"])){		// Determining if a parent category is used
						$epvalue = ",".$row[$r]["parent_id"];	
						if($epvalue==","){$epvalue = ",0";}	
					}
					else{$epvalue = "";}
					
					$jsedit = "edititem(";
					$jsedit .= $row[$r]["prim_key"].",'".str_replace("'","\'",$row[$r]["list_value"])."',".$row[$r]["ordering"].$epvalue;
					$jsedit .= ");";
				?>
                <img src="<?php echo $compbase."images/" ?>edit16.png" alt="Edit" title="Edit this record" onclick="<?php echo $jsedit; ?>" /> 
                <img src="<?php echo $compbase."images/" ?>remove16.png" alt="Edit" title="Delete this record" onclick="delitem(<?php echo $row[$r]["prim_key"].",'".str_replace("'","\'",$row[$r]["list_value"])."'"?>)" />
               
            </td>
        </tr>
<?php
		}
?>  
		</tbody> 
  	</table>  
    <script>
		function edititem(pk, evalue,eorder<?php if(isset($_REQUEST["parent_name"])){echo ",epvalue";} ?>){
			$("#edit_prim_key").val(pk);
			$("#edit_list_value").val(evalue);
			$("#edit_ordering").val(eorder);
			<?php if(isset($_REQUEST["parent_name"])){ ?>
			$("#edit_parent_id").val(epvalue);
			<?php } ?>
			
			$( "#edit_dialog" ).dialog( "open" );
			return false;
		}
		
		function delitem(pk, evalue){
			$("#del_prim_key").val(pk);
			$("#delname").html(evalue);
			
			$( "#del_dialog" ).dialog( "open" );
			//return false;
		}
	</script>
    <br /><br />
    <div class="jbtn">
		<a href="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=setup"><img src="<?=$compbase?>images/back_arrow16.png" align="absmiddle" /> Return to List</a> 
    </div>


    <div id="add_dialog" title="Add <?php echo $_REQUEST["list_name"]; ?>">
        <center>
            <form name="frm_add" method="post">
                <br />
                <input type="hidden" value="<?php echo $_REQUEST["list_name"]; ?>" name="add[list_category]" id="list_category" />
                <?php echo $_REQUEST["list_name"]; ?>: <input type="text" value="" name="add[list_value]" id="list_value" />
                
				<?php
				if(isset($_REQUEST["parent_name"]))
				{
				?>
                <br /><br />
                <?php echo $_REQUEST["parent_name"]; ?>: 
                <select name="add[parent_id]" id="parent_id">
                	<option value="">Select <?php echo $_REQUEST["parent_name"]; ?> ...</option>
					<?php echo getdroplist($_REQUEST["parent_name"], $selectval ="", $prim_as_select = true) ?>
                </select>
                <?php } ?>
                <br /><br />
                Order: <input type="text" value="0" name="add[ordering]" id="ordering" />
                
                <input type="hidden" value="add" name="action" id="action" />
                <?php if(isset($_REQUEST["parent_name"])){$plink = "&parent_name=".str_replace(" ","+",$_REQUEST["parent_name"]);}else{$plink = "";} ?>
                <input type="hidden" value="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=setup&list_name=<?php echo $_REQUEST["list_name"].$plink; ?>" name="returnurl" id="returnurl" />
    			<br /><br />
                <div class="jbtn"><input type="submit" value="Save <?php echo $_REQUEST["list_name"]; ?>"  /></div>
            </form>
        </center>
    </div>



    <div id="edit_dialog" title="Edit <?php echo $_REQUEST["list_name"]; ?>">
        <center>
            <form name="frm_edit" method="post">
                <br />
                <input type="hidden" value="<?php echo $_REQUEST["list_name"]; ?>" name="add[list_category]" id="list_category" />
				<input type="hidden" value="" name="prim_key" id="edit_prim_key" />
                <?php echo $_REQUEST["list_name"]; ?>: <input type="text" value="" name="add[list_value]" id="edit_list_value" />
                
				<?php
				if(isset($_REQUEST["parent_name"]))
				{
				?>
                <br /><br />
                <?php echo $_REQUEST["parent_name"]; ?>: 
                <select name="add[parent_id]" id="edit_parent_id">
                	<option value="">Select <?php echo $_REQUEST["parent_name"]; ?> ...</option>
					<?php echo getdroplist($_REQUEST["parent_name"], $selectval ="", $prim_as_select = true) ?>
                </select>
                <?php } ?>
                <br /><br />
                Order: <input type="text" value="0" name="add[ordering]" id="edit_ordering" />
                
                <input type="hidden" value="edit" name="action" id="action" />
                <?php if(isset($_REQUEST["parent_name"])){$plink = "&parent_name=".str_replace(" ","+",$_REQUEST["parent_name"]);}else{$plink = "";} ?>
                <input type="hidden" value="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=setup&list_name=<?php echo $_REQUEST["list_name"].$plink; ?>" name="returnurl" id="returnurl" />
    			<br /><br />
                <div class="jbtn"><input type="submit" value="Save <?php echo $_REQUEST["list_name"]; ?>"  /></div>
            </form>
        </center>
    </div>




    <div id="del_dialog" title="Edit <?php echo $_REQUEST["list_name"]; ?>">
        <center>
            <form name="frm_del" method="post">
                <br />
				<input type="hidden" value="" name="prim_key" id="del_prim_key" />        
                Are you sure you want to delete <strong><span id="delname"></span></strong>?
                
                <input type="hidden" value="delete" name="action" id="action" />
                <input type="hidden" value="index.php?option=<?php echo $dconfig["comp_name"]; ?>&view=setup&list_name=<?php echo $_REQUEST["list_name"].$plink; ?>" name="returnurl" id="returnurl" />
    			<br /><br />
                <div class="jbtn"><input type="submit" value="Confirm Delete"  /></div>
            </form>
        </center>
    </div>

    
</div>


<?php
	}
?>

<form method="post" name="adminForm" id="adminForm">
            <input type="hidden" name="task" value="" />
</form>