<?php
	function getdbconfig()
	{	//Used to get configuration data from the database
		//Returns an array with the configuration
		$db = JFactory::getDBO();
		$db->setQuery("SELECT * FROM #__vlm_config");
		$row = $db->loadAssocList();
		
		for($r = 0; $r < count($row); $r++){
			$outarr[$row[$r]["soption"]] = $row[$r]["svalue"];
		}
		return $outarr;
	}

	

	
	function getdroplist($listcat, $selectval ="", $prim_as_select = true){
		//$listcat			=	string		Category to get data from		
		//$selectval		=	string		Value if found to be selected in the drop list
		//$prim_as_select	=	boolean		If true, the prim_key will be used as the option value, else, the display name
		$db =& JFactory::getDBO();
		$query = "SELECT prim_key, list_value FROM #__vlm_droplistvalues WHERE list_category = '$listcat' ORDER BY ordering, list_value";
		$db->setQuery($query);
		$row = $db->loadAssocList();
		$ret_list = "";
		for($r=0; $r < count($row); $r++){
			if($prim_as_select){ 
				//Using display name as value
				if($row[$r]["prim_key"] == $selectval){$sel = "selected=\"selected\"";}else{$sel = "";};
				$ret_list .= "\t<option value=\"".$row[$r]["prim_key"]."\" $sel>".$row[$r]["list_value"]."</option>/n";
			}else{ 
				//Using primary key as value
				if($row[$r]["list_value"] == $selectval){$sel = "selected=\"selected\"";}else{$sel = "";};
				$ret_list .= "\t<option value=\"".$row[$r]["list_value"]."\" $sel>".$row[$r]["list_value"]."</option>/n";
			}
		}
		return $ret_list;
	}
	
		function getmultilist($listcat, $selectval, $prim_as_select = true){
		//$listcat			=	string		Category to get data from		
		//$selectval		=	array		Value(s) if found to be selected in the drop list
		//$prim_as_select	=	boolean		If true, the prim_key will be used as the option value, else, the display name
		$db =& JFactory::getDBO();
		$query = "SELECT prim_key, list_value FROM #__vlm_droplistvalues WHERE list_category = '$listcat' ORDER BY ordering, list_value";
		$db->setQuery($query);
		$row = $db->loadAssocList();
		$ret_list = "";
		for($r=0; $r < count($row); $r++){
			if($prim_as_select){ 
				//Using display name as value
				if(in_array($row[$r]["prim_key"],$selectval)){$sel = "selected=\"selected\"";}else{$sel = "";};
				$ret_list .= "\t<option value=\"".$row[$r]["prim_key"]."\" $sel>".$row[$r]["list_value"]."</option>\n";
			}else{ 
				//Using primary key as value
				if(in_array($row[$r]["list_value"],$selectval)){$sel = "selected=\"selected\"";}else{$sel = "";};
				$ret_list .= "\t<option value=\"".$row[$r]["list_value"]."\" $sel>".$row[$r]["list_value"]."</option>\n";
			}
		}
		return $ret_list;
	}

	function make_thumb($src,$dest,$desired_width)
	{	//This function is used to create thumb nails	
	  /* read the source image */
	  $source_image = imagecreatefromjpeg($src);
	  $width = imagesx($source_image);
	  $height = imagesy($source_image);
	  
	  /* find the "desired height" of this thumbnail, relative to the desired width  */
	  $desired_height = floor($height*($desired_width/$width));
	  
	  /* create a new, "virtual" image */
	  $virtual_image = imagecreatetruecolor($desired_width,$desired_height);
	  
	  /* copy source image at a resized size */
	  imagecopyresized($virtual_image,$source_image,0,0,0,0,$desired_width,$desired_height,$width,$height);
	  
	  /* create the physical thumbnail image to its destination */
	  imagejpeg($virtual_image,$dest);
	}
	
	
	
?>