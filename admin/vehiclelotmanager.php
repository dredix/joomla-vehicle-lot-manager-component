<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
include("functions.php");
$vconfig = getdbconfig();

include("helpers/redirect.php");		//Checks task and redirects if need be



$dconfig["baseurl"] = JURI::base();
$dconfig["comp_name"] = $vconfig["comp_name"];
$dconfig["jtheme"] = $vconfig["back_jquey_theme"];
$dconfig["ver"] = $vconfig["version"];
$compbase = $dconfig["baseurl"]."components/".$dconfig["comp_name"]."/";

require("cls/Database.php");

// import Joomla view library
jimport('joomla.application.component.view');

// load tooltip behavior
JHtml::_('behavior.tooltip');
$db =& JFactory::getDBO();						//Used to database handling		http://docs.joomla.org/Accessing_the_database_using_JDatabase
$document =& JFactory::getDocument();			//Used to add javascript and css to pages	 http://docs.joomla.org/JDocument/addScript
$config =& JFactory::getConfig();				//Used to joomla related configuration
$app =& JFactory::getApplication(); 			//Used to display messages		http://docs.joomla.org/Display_error_messages_and_notices
	
$ddb = Database::obtain($config->getValue('config.host'), $config->getValue('config.user'),$config->getValue('config.password'), $config->getValue('config.db'));		// create the $db singleton object
$ddb->connect();		// connect to the server
	
	include("helpers/submenu.php");	//Adding submenu buttons
	
	$document->addStyleSheet($compbase."css/buttons.css");
	
	$view = $_REQUEST["view"];
	switch ($view)
	{
		case "vehicles":
		  include("views/vehicles.php");
		  break;
		case"addvehicles":
		  include("views/vehicle_add.php");
		  break;
		case "editvehicles":
		  include("views/vehicle_add.php");
		  break;
		case "about":
		  include("views/about.php");
		  break;
		case "config":
		  include("views/config.php");
		  break;
		case "setup":
		  include("views/setup.php");
		  break;
		case "reports":
		  include("views/reports.php");
		  break; 
		case "cpanel":
		  include("views/cpanel.php");
		  break;  
		default:
		  include("views/cpanel.php");
	}
?>

<br /><br />

<!---
<h2>Request Data</h2>
<textarea style="width:90%; height:120px">
<?php print_r($vconfig); /*print_r($_REQUEST);*/ ?>
</textarea>
--->