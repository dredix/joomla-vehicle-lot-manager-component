<?php
	$document->addScript($compbase."js/lightbox.js");
	$document->addStyleSheet($compbase."css/lightbox.css");
	$document->addStyleSheet($compbase."css/main.css");
	
	if (!function_exists("getalias")){
		function getalias(){
			$menu = JSite::getMenu();
			return $menu->getActive()->alias;  // alias of active menu
		}
	}
	
	
	if(isset($_REQUEST["action"])){
		if($_REQUEST["action"] == "compareset"){
			if($_REQUEST["compare_value"] == 1){
				$_SESSION["vcompare"][$_REQUEST["compare_vid"]] = $_REQUEST["compare_vid"];
			}else{
				unset($_SESSION["vcompare"][$_REQUEST["compare_vid"]]);
			}
		}
	}
	

?>

<div id="vlist">
<?

$db->setQuery($vlistquery);
$row = $db->loadAssocList();

if($showcount == 1){
echo "Total Vehicle(s): ".count($row)."<br />";
}

for($r=0; $r < count($row); $r++)
{
	$userfeature_cnt = 5;			//Maximum count of features that should be displayed.
	$featurearr = explode(";",$row[$r]["features"]);
	
?>




    <div class="vlistitem">
    	<a href="index.php/<?=$vconfig["main_url_segment"]?>?option=<?=$dconfig["comp_name"]."&vview=singlevehicle&id=".$row[$r]["vehicle_id"]?>" target="_self">
            <table>
                <tr>
                    <td class="imagep">
                        <?php
							$mainimg = explode(";",$row[$r]["vimage"]);
							if($mainimg[0]!=""){
								if(file_exists($vconfig["thumbnail_dir"].$row[$r]["vehicle_id"].".jpg")){
									$main_img = $vconfig["thumbnail_dir"].$row[$r]["vehicle_id"].".jpg";
								}else{
									$main_img = $mainimg[0];
								}
								//$main_img = $vconfig["thumbnail_dir"].$row[$r]["vehicle_id"].".jpg"
						?>
                    	<a href="<?=JURI::base().$mainimg[0]?>" rel="lightbox[vpreview]"><img src="<?=$main_img?>" class="imgpreview" alt="No Image" title="<img src='<?=JURI::base().$mainimg[0]?>' style='width:250px; height:auto' />" /></a>
                        <?php }else{ ?>
                        <img src="<?=$compbase?>images/comingsoon88.jpg" class="imgpreview" alt="Image Coming Soon" title="Images coming soon" />
                        <?php } ?>
                    </td>
                    <td class="tbl_info">
                    	<?php
							if($_SESSION["vcompare"][$row[$r]["vehicle_id"]] == $row[$r]["vehicle_id"]){
								$vcompare = 0;
								$compare_img = "checkbox.gif";
							}else{
								$vcompare = 1;
								$compare_img = "unchecked.gif";
							}
						?>
                    	<a href="javascript: setcompare(<?=$row[$r]["vehicle_id"]?>, <?=$vcompare?>);">
                    	<span class="compare"><img src="<?=$compbase?>images/<?=$compare_img?>" alt="Watching" align="absmiddle" /> Compare</span>
                        </a>
                        
                    	<!--<img src="<?=$compbase?>images/star_grey.png" title="This vehicle is on your watch list" class="liststar" alt="Watching" /> -->
                      <h3><?=$row[$r]["vtitle"]?></h3>
                        <p class="specs"><?=$row[$r]["vyear"]?> <?=$row[$r]["vmake_name"]?> <?=$row[$r]["vmodel_name"]?> <?php if($row[$r]["vmodel_name"] != ""){ echo "(".$row[$r]["vehicle_type"].")";} ?></p>
                        <p class="specs">
                            <?php
								if($row[$r]["colour_ext"] != ""){echo $row[$r]["colour_ext"].", ";}
								if($row[$r]["doors"] != ""){echo $row[$r]["doors"]." Doors, ";}
								if($row[$r]["fuel_type"] != ""){echo $row[$r]["fuel_type"].", ";}
								if($row[$r]["engine"] != ""){echo $row[$r]["engine"].", ";}
								if($row[$r]["transmission"] != ""){echo $row[$r]["transmission"]." Transmission, ";}
							?>
                        </p>
                        
                        <p class="featurelist">
                            <?php
								//Determining if feature count exceeds max feature cout setting.
								if (count($featurearr) > $userfeature_cnt){$maxfs = $userfeature_cnt;}
								else {$maxfs = count($featurearr);}
								
                                for($f = 0; $f < $maxfs; $f++){
                                    if($f == 0){echo $featurearr[$f]."";}
                                    else {echo ", ".$featurearr[$f]."";}
                                }
                            ?>
                        </p>
                    </td>
                    <td class="tbl_moneinfo">
                    	<p class="price"><?=$row[$r]["price_unit"]?> <?=$vconfig["money_sign"]?><?=$row[$r]["price"]?></p>
                    	<? if($vconfig["show_consession"]==1 && $row[$r]["consession_price"] != "" && $row[$r]["consession_price"] > 0):?><p class="cprice"><?=$row[$r]["price_unit"]?> <?=$vconfig["money_sign"]?><?=$row[$r]["consession_price"]?></p><? endif; ?>
                        <p class="mileage"><?=$row[$r]["mileage_unit"]?> <?=$row[$r]["mileage"]?></p>
                        <? if($row[$r]["featured"]==1):?><p class="vfeatured"><img src="<?=$acompbase?>images/featured.png" align="absmiddle" /> Featured</p><? endif; ?>
                        <? if($_COOKIE["viewid_".$row[$r]["vehicle_id"]]==1):?><p><span class="viewed" title="This means you have viewed this vehicle at some point in the past."><img src="<?=$compbase?>images/view.png" align="absmiddle" /> Viewed</span></p><? endif; ?>
                      	
                    </td>
                </tr>
            </table>
        </a>
    </div>

<?php
}
?>
	<form method="post" id="comparedata">
    	<input type="hidden" name="compare_vid" id="compare_vid" />
        <input type="hidden" name="compare_value" id="compare_value" />
        <input type="hidden" name="action" value="compareset" />
    </form>
    <script>
		function setcompare(vid, cvalue){
			$("#compare_vid").val(vid);
			$("#compare_value").val(cvalue);
			
			document.forms["comparedata"].submit();
		}
	</script>
    
</div>