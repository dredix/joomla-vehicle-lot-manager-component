<?php
	$document->addScript($acompbase."js/jquery.ui.core.js");
	$document->addScript($acompbase."js/jquery-ui-1.8.18.custom.js");
	$document->addScript($acompbase."js/jquery.ui.widget.js");
	$document->addScript($acompbase."js/jquery.ui.tabs.js");
	$document->addScript($acompbase."js/jquery.ui.dialog.js");
	$document->addScript($compbase."js/jquery.timers-1.2.js");	
	//$document->addScript($compbase."js/jquery.easing.1.3.js");	
	$document->addScript($compbase."js/jquery.galleryview-3.0-dev.js");	//Used for image gallery
	$document->addScript($compbase."js/loan_calc.js");
	
	
	$document->addStyleSheet($compbase."css/jquery.galleryview-3.0-dev.css");
	$document->addStyleSheet($compbase."css/loan_style.css");
	$document->addStyleSheet($compbase."css/main.css");
	
	
	//Incrementinf Hits
	$query = "UPDATE #__vlm_vehicle h SET hits = hits + 1 WHERE vehicle_id = ".$db->Quote($db->Escape($_REQUEST[id]));
	$db->setQuery($query);
	$db->query();


	$query = "SELECT i.*, m.list_value as vmake_name, v.list_value as vmodel_name
	, (SELECT t.list_value FROM #__vlm_droplistvalues t WHERE prim_key = i.vtype) as vehicle_type
FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
ON i.vmake = m.prim_key
WHERE vehicle_id = ".$db->Quote($_REQUEST["id"]).";";


	$cookieexpire=time()+60*60*24*$vconfig["vview_expire"];	//Setting how many days the cookie should expire
	setcookie("viewid_".$_REQUEST["id"], "1", $cookieexpire);
	
	$db->setQuery($query);
	$row = $db->loadAssoc();
	
	
	//Setting main vehicle image
	$totalimages = 0;	//Stores total images
	$vimages = explode(";",$row["vimage"]);
	for($i = 0; $i <count($vimages); $i++){
		if(trim($vimages[$i] ) != ""){$totalimages++;}	//Imcrementing if image exist
	}

	if(file_exists($vconfig["thumbnail_dir"].$row["vehicle_id"].".jpg")){
		$mainimg = $vconfig["thumbnail_dir"].$row["vehicle_id"].".jpg";
	}else{
		$mainimg = $vimages[0];
	}
	
	
	if(isset($_REQUEST["action"])){
		if($_REQUEST["action"] == "compareset"){
			if($_REQUEST["compare_value"] == 1){
				$_SESSION["vcompare"][$_REQUEST["compare_vid"]] = $_REQUEST["compare_vid"];
			}else{
				unset($_SESSION["vcompare"][$_REQUEST["compare_vid"]]);
			}
		}
		
		if($_REQUEST["action"] == "sendvinfo"){
			$mailer =& JFactory::getMailer();
			//Getting sender info
			$config =& JFactory::getConfig();
			$sender = array( 
				$config->getValue( 'config.mailfrom' ),
				$config->getValue( 'config.fromname' ) ); 
			$mailer->setSender($sender);	//Setting sender
			
			$recipient = $_REQUEST["si_email"];
			$mailer->addRecipient($recipient);
			
			$btncss = "background-color: #E9EFF5; padding; 10px; border-radius:5px; text-decoration: none; border: 2px solid #8CACBB; margin-bottom: 5px";
			
			$body   = '<h2 style="background-color:#E9EFF5; border-top: 2px solid #8CACBB">'.$row["vtitle"].'</h2>'
				."You sent a link for this vehicle from <a href=\"".JURI::base()."\">".JURI::base()."</a>.<br>\n
				See the details below.<br>"
				. '<img src="'.JURI::base().$mainimg.'" style="float:right; margin: 5px; padding: 3px; border: 1px solid black" /> '
				. "<p><strong>Description</strong><br>".$row["description"]."</p>"
				
				."<div><strong>Speciification</strong><br>"
				."<table>"
				."<tr><th>Price</th><td>".$row["price_unit"]." ".$vconfig["money_sign"].$row["price"]."</td></tr>"
				."<tr><th>Make</th><td>".$row["vmake_name"]."</td></tr>"
				."<tr><th>Model</th><td>".$row["vmodel_name"]."</td></tr>"
				."<tr><th>Year</th><td>".$row["vyear"]."</td></tr>"
				."<tr><th>Type</th><td>".$row["vehicle_type"]."</td></tr>"
				."<tr><th>Condition</th><td>".$row["vcondition"]."</td></tr>"
				."<tr><th>Mileage</th><td>".$row["mileage"]." ".$row["mileage_unit"]."</td></tr>"
				."<tr><th>Exterior Colour</th><td>".$row["colour_ext"]."</td></tr>"
				."</table>"
				."</div>"
				
				."<a href=\"".JURI::base()."index.php/".$vconfig["main_url_segment"]."?vview=singlevehicle&id=".$row["vehicle_id"]."\" style='$btncss'>Click here for full vehicle details</a><br><br>"
				."<a href=\"".JURI::base()."\" style='$btncss'>Check our the website for more vehicles.</a>"
				
				. "";
					 
			$body .= '<br><br><span style="font-size: 0.9em; color: gray">Powered by by Vehicle Lot Manager by <a href="http://www.dredix.net">Dredix</a></span>';
			
			$mailer->setSubject('Vehicle Details about '.$row["vtitle"]);
			$mailer->isHTML(true);
			$mailer->Encoding = 'base64';
			$mailer->setBody($body);
			// Optionally add embedded image
			$mailer->AddEmbeddedImage( JURI::base().$mainimg, 'vimg_id', 'vimg.jpg', 'base64', 'image/jpeg' );
			if ($_POST['si_code'] == $_SESSION['captcha']) {
				if ( $mailer->Send() ) {
					$app = JFactory::getApplication();
					$app->enqueueMessage( 'Vehicle information has been sent to the email address.' );
				} else {
					JError::raiseWarning( 100, 'Error sending email: ' . $send->message );
				}
			}else{
				JError::raiseWarning( 100, 'The code you entered to send vehicle information is invalid.' . $send->message );
			}
		}
	}
	
	$_SESSION['captcha'] = mt_rand(10000, 99999);	//Generating new captcha code

	$document->setTitle( $row["vtitle"] );
	
	//Adding breadcrumb information
	$mainframe = &JFactory::getApplication();	
	$pathway   =& $mainframe->getPathway();	
	$pathway->addItem($row["vtitle"] , '');
?>

<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>


<div id="maininfo">
    <div class="vmainimg">
    	<?php if(trim($vimages[0]) != ""){ ?>
		<script>
			function openiviewer(){
				$( "#mg2_dialog" ).dialog( "open" );	
			}
		</script>
        <?php
			if(file_exists($vconfig["thumbnail_dir"].$row["vehicle_id"].".jpg")){
				$mainimg = $vconfig["thumbnail_dir"].$row["vehicle_id"].".jpg";
			}else{
				$mainimg = $vimages[0];
			}
		?>
    	<a href="javascript: openiviewer();" title="Click to view all images">
        	<img src="<?=$mainimg?>" style="width:<?php if($vconfig["svv_mainimagesize"] != ""){echo $vconfig["svv_mainimagesize"];}else{echo "150";} ?>px; height:auto" />
        </a>
        <?php } else { ?>
        <img src="<?=$compbase?>images/comingSoon400.jpg" style="width:<?php if($vconfig["svv_mainimagesize"] != ""){echo $vconfig["svv_mainimagesize"];}else{echo "150";} ?>px; height:auto" />
        <?php } ?>
        <?php
			//make_thumb($vimages[0],"images/vlm_tn/".$row["vehicle_id"].".jpg",250);
		?>
        
        
    </div>
    <div class="vmaininfo">
        <h3><?=$row["vtitle"]?></h3>
        
        <?=$row["description"]?>
	   	<?php
            if($_SESSION["vcompare"][$row["vehicle_id"]] == $row["vehicle_id"]){
                $vcompare = 0;
                $compare_img = "checkbox.gif";
            }else{
                $vcompare = 1;
                $compare_img = "unchecked.gif";
            }
        ?>
        <a href="javascript: setcompare(<?=$row["vehicle_id"]?>, <?=$vcompare?>);">
        <span class="compare"><img src="<?=$compbase?>images/<?=$compare_img?>" alt="Watching" align="absmiddle" /> Compare</span>
        </a>
        <? if($row["featured"]==1):?><p class="vfeatured"><img src="<?=$acompbase?>images/featured.png" align="absmiddle" /> Featured</p><? endif; ?>
        <? if($_COOKIE["viewid_".$_REQUEST["id"]]==1):?><p><span class="viewed" title="This means you have viewed this vehicle at some point in the past."><img src="<?=$compbase?>images/view.png" align="absmiddle" /> Viewed</span></p><? endif; ?>

    </div>
</div>

<div class="clr"></div>

<div id="otherinfo">
<div id="tabs">
	<ul>
		<li><a href="#voverview">Overview</a></li>
        <?php if($totalimages > 0){ ?><li><a href="#vimagegallery">Images</a></li><?php } ?>
		<?php if($vconfig["show_loancalc"] == 1){ ?><li><a href="#loan_calc">Loan Calculator</a></li><?php } ?>
        <?php if($vconfig["svv_showreviews"] == 1){ ?><li><a href="#vreviews">Reviews</a></li><?php } ?>
        <li><a href="#vsendinfo">Send Information</a></li>
	</ul>
	<div id="voverview">
    	<h3>Pricing Information</h3>
        <dl>
            <dt>Price</dt>
            <dd><?=$row["price_unit"]." ".$vconfig["money_sign"].$row["price"]?></dd>
            <?php if($vconfig["show_consession"]=="1" && $row["consession_price"] > 0){ ?>
            <dt>Consession Price</dt>
            <dd><?=$row["price_unit"]." ".$vconfig["money_sign"].$row["consession_price"]?></dd>
            <?php } ?>
            <dt>Price Type</dt>
            <dd><?=$row["price_type"]?></dd>
        </dl>
    
		<h3>Specifications</h3>
        <table class="tdivider" style="border:none">
        	<tr>
            	<td>
                    <dl>
                        <dt>Make</dt>
                        <dd><a href="#" title="See more vehicles of this make"><?=$row["vmake_name"]?></a></dd>
                        <dt>Model</dt>
                        <dd><a href="#" title="See more vehicles of this model"><?=$row["vmodel_name"]?></a></dd>
                        <dt>Year</dt>
                        <dd><a href="#" title="See more vehicles of this year"><?=$row["vyear"]?></a></dd>
                        <dt>Condition</dt>
                        <dd><?=$row["vcondition"]?></dd>
                        <?php if($row["vehicle_type"] !=""){ ?><dt>Vehicle Type</dt><dd><?=$row["vehicle_type"]?></dd><?php } ?>
                        <?php if($row["vin_no"] !=""){ ?><dt>Vin #</dt><dd><?=$row["vin_no"]?></dd><?php } ?>
                    </dl>
                </td>
                <td>
                    <dl>
                        <dt>Mileage</dt>
                        <dd><?=$row["mileage"]?> <?=$row["mileage_unit"]?></dd>
                        <?php if($row["mpg"] !=""){ ?><dt>Estimage MPG</dt><dd><?=$row["mpg"]?></dd><?php } ?>
                        <dt>Exterior Colour</dt>
                        <dd><?=$row["colour_ext"]?></dd>
                        <dt>Interior Colour</dt>
                        <dd><?=$row["colour_int"]?></dd>
                        
                        <?php if($row["doors"] !=""){ ?><dt>Doors</dt><dd><?=$row["doors"]?></dd><?php } ?>
                        <dt>Engine</dt><dd><?=$row["engine"]?></dd>
                        <?php if($row["fuel_type"] !=""){ ?><dt>Fuel Type</dt><dd><?=$row["fuel_type"]?></dd><?php } ?>
                        <?php if($row["transmission"] !=""){ ?><dt>Tranmission</dt><dd><?=$row["transmission"]?></dd><?php } ?>
                    </dl>
                </td>
            </tr>
        </table>
        <?php
			if(isset($row["features"]))
			{
			$feature = explode(";",$row["features"]);
			
		?>
        <h3>Features</h3>
 		
        <table class="tdivider" style="border:none">
        	<tr>
            	
            	<?php
					for($f=0; $f<count($feature); $f++){
						echo "<td><img src=\"".$compbase."images/".$vconfig["feature_bullet"]."\" alt='-' align='absmiddle' /> ".$feature[$f]."</td>";
						if(($f%2) == 1){
							echo "</tr><tr>";
						}
					}
				?>
                
            </tr>
        </table>
        <?php
			}
		?>


		<?php
			if(isset($row["contact_info"]) && trim($row["contact_info"]) != "")
			{
		?>
        <h3>Contact Information</h3>
 		
        <p><?=$row["contact_info"]?></p>
        <?php
			}
		?>
	</div>
    
    <?php if($vconfig["show_loancalc"] == 1){ ?>
    <div id="loan_calc">
		<div class="loan">
        	Please note that this loan calculator is only an estimate.
            <div class="clr"></div>
			<div class="loan_src" id="srcamt">
				
                <form name="loan_form">
                    <table align='center' style="margin:10px">
                    <tr>
    
                        <td>Loan Amount:</td>
                        <td><input type="text" name="amt" size="10" value="<?=$row["price"]?>" title="How much are you planning to borrow?" /></td>
                    </tr>
                    <tr>
                        <td>Number of Payments:</td>
                        <td><input type="text" name="pay" size="10" title="How many months do you need to repay this loan?.<br />12 = 1 year, 24 = 2 year, etc.." /></td>
                    </tr>
                    <tr>
    
                        <td>Annual Interest Rate:</td>
                        <td><input type="text" name="rate" size="10" value="<?=$vconfig["calc_interestrate"]?>" title="What is the interest rate the bank is currently charging?<br />(ex. 8.5% = 8.5)" /></td>
                    </tr>
                    <tr>
                        <td colspan='2' align='center' style='padding-top:5px'>
                            <input type="button" class="button" onClick="return check()" value="  Calculate  " title="Click to calculate loan" />&nbsp;&nbsp;&nbsp;
                            <input type="button" class="button" onClick="clearScreen()" value="Start Over" />
                        </td>
    
                    </tr>
                    </table>
				</form>
			</div>
			<div class="loan_pmt" id="pmt"></div>
			<div class="loan_out" id="det"></div>
			<!--[if IE 6]>
			<script>document.all.srcamt.style.width="235px";document.all.pmt.style.width="235px";</script>
			<![endif]-->
		</div>
	</div>
    <?php } ?>
    
    <?php if($vconfig["svv_showreviews"] == 1){ ?>
    <div id="vreviews">
		<p>Vehicle review information here</p>
	</div>
    <?php } ?>
    
	<div id="vsendinfo">
    	<img src="<?=$compbase ?>images/e-mail2.png" style="float:right" />
		<form method="post">
        	<input type="hidden" name="action" value="sendvinfo" />
            <dl>
                <dt>Your Full Name</dt>
                <dd><input type="text" name="si_fullname" /></dd>
                <dt>Your email address</dt>
                <dd><input type="text" name="si_email" /></dd>
                <dt>Enter this number: <?=$_SESSION['captcha'] ?></dt>
                <dd><input type="text" name="si_code" /></dd>
            </dl> 
            
            <div class="jbtn"><input type="submit" value="Send me this vehicle information" /></div>
        </form>
	</div>
    
    <?php if($totalimages > 0){ ?>
    <div id="vimagegallery">
        <ul id="myGallery">
            
			<?php
            
			echo "Images: ".count($vimages)."<br />";
			for($i = 0; $i <count($vimages); $i++){
				if(trim($vimages[$i] ) != ""){
            ?>
            <li>
            <img src="<?=$vimages[$i]?>" title="Lone Tree Yellowstone" />
            </li>
            <?php }} ?>

        </ul>
		<script>
			$('#myGallery').galleryView({
				<?php if(isset($vconfig["svv_gallery_width"])){?>panel_width: <?=$vconfig["svv_gallery_width"]?>,<?php } ?>
				filmstrip_style: 'showall',
				transition_speed: 250,
				filmstrip_position: 'bottom',
				frame_height: 50,
				frame_width: 80,
				frame_opacity: 0.6
			});
		</script>
    </div>
    <?php } ?>
    
</div>

</div>



<div id="mg2_dialog" title="<?=htmlentities($row["vtitle"])?> Images">
    <br />
	<center>
    <ul id="myGallery2">
        
        <?php
        
        echo "Images: ".count($vimages)."<br />";
        for($i = 0; $i <count($vimages); $i++){
            if(trim($vimages[$i] ) != ""){
        ?>
        <li>
        <img src="<?=$vimages[$i]?>" title="Lone Tree Yellowstone" />
        </li>
        <?php }} ?>

    </ul>
    </center>
    <script>
        $('#myGallery2').galleryView({
            <?php if(isset($vconfig["svv_gallery_width"])){?>panel_width: <?=$vconfig["svv_gallery_width"]?>,<?php } ?>
            filmstrip_style: 'showall',
            transition_speed: 250,
            filmstrip_position: 'right',
            frame_height: 50,
            frame_width: 80,
            frame_opacity: 0.6
        });
    </script>
</div>

	<script>
	// increase the default animation speed to exaggerate the effect
	$.fx.speeds._default = 1000;
	$(function() {
		$( "#mg2_dialog" ).dialog({
			autoOpen: false,
			show: "blind",
			height: 500,
			width: 1000,
			modal: true
		});
	});
	</script>
    
	<form method="post" id="comparedata">
    	<input type="hidden" name="compare_vid" id="compare_vid" />
        <input type="hidden" name="compare_value" id="compare_value" />
        <input type="hidden" name="action" value="compareset" />
    </form>
    <script>
		function setcompare(vid, cvalue){
			$("#compare_vid").val(vid);
			$("#compare_value").val(cvalue);
			
			document.forms["comparedata"].submit();
		}
	</script>