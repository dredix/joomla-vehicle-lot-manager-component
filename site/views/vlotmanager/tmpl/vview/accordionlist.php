<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>

	<div id="wrapper">
<?php

		$document->addScript($compbase."js/simple_accord.js");
		$document->addStyleSheet($compbase."css/accordion.css");
		
		//$query = "SELECT prim_key, list_value FROM #__vlm_droplistvalues WHERE list_category = 'Make' ORDER BY list_value";
		$query = "SELECT prim_key, list_value, (SELECT count(vehicle_id) FROM #__vlm_vehicle v WHERE vmake = h.prim_key) AS vcount
					FROM #__vlm_droplistvalues h WHERE list_category = 'Make' ORDER BY list_value";
		$db->setQuery($query);
		$row = $db->loadAssocList();
		
		//echo "<ul>\n";
		for($r=0; $r < count($row); $r++){
			echo "\n\t<div class=\"accordionButton\">".$row[$r]["list_value"]."</div>";
			echo "\n\t<div class=\"accordionContent\">";
			if($row[$r]["vcount"] > 0){
				echo "\n\t\t<ul>";
				echo "\n\t\t\t<li><a href='?option=".$dconfig["comp_name"]."&vview=singlemake&makeid=".$row[$r]["prim_key"]."'><strong>All ".$row[$r]["list_value"]." (".$row[$r]["vcount"].")</strong></a></li>";
				//echo "\t<li><a href='?option=".$dconfig["comp_name"]."&vview=singlemake&makeid=".$row[$r]["prim_key"]."'><strong>".$row[$r]["list_value"]."</strong></a></li>\n";
				//$subquery = "SELECT prim_key, list_value FROM #__vlm_droplistvalues WHERE parent_id = ".$row[$r]["prim_key"]." ORDER BY list_value";
				$subquery = "SELECT prim_key, list_value, (SELECT count(vehicle_id) FROM #__vlm_vehicle v WHERE vmodel = h.prim_key) AS vcount
							FROM #__vlm_droplistvalues h WHERE parent_id = ".$row[$r]["prim_key"]." ORDER BY list_value";
				$db->setQuery($subquery);
				$row2 = $db->loadAssocList();
				for($r2=0; $r2 < count($row2); $r2++){
					//echo "\t\t<ul>\n";
					echo "\n\t\t\t<li><a href='?option=".$dconfig["comp_name"]."&vview=singlemodel&modelid=".$row2[$r2]["prim_key"]."'>".$row2[$r2]["list_value"]." (".$row2[$r2]["vcount"].")</a></li>\n";
					//echo "\t\t</ul>\n";
				}
				echo "\n\t\t</ul>";
			}else{
				echo "Not vehicles currently available in this category.";	
			}
			echo "\n\t</div>";
		}
		//echo "</ul>\n";
?>
	</div>
