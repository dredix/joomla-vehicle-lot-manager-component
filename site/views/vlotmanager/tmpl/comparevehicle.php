<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$dconfig["comp_name"] = "com_vehiclelotmanager";
$dconfig["baseurl"] = JURI::base();

$compbase = $dconfig["baseurl"]."components/".$dconfig["comp_name"]."/";
$acompbase = $dconfig["baseurl"]."administrator/components/".$dconfig["comp_name"]."/";


function getdbconfig()
{
	$db =& JFactory::getDBO();
	$db->setQuery("SELECT * FROM #__vlm_config");
	$row = $db->loadAssocList();
	
	for($r = 0; $r < count($row); $r++){
		$outarr[$row[$r]["soption"]] = $row[$r]["svalue"];
	}
	return $outarr;
}
$vconfig = getdbconfig();
$dconfig["jtheme"] = $vconfig["front_jquery_theme"];


$db =& JFactory::getDBO();						//Used to database handling		http://docs.joomla.org/Accessing_the_database_using_JDatabase
$document =& JFactory::getDocument();


$document->addStyleSheet($acompbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
$document->addStyleSheet($compbase."css/"."main.css");
//$document->addStyleSheet($compbase."css/accordion.css");
$document->addScript($acompbase."js/jquery-1.7.1.js");
//$document->addScript($compbase."js/simple_accord.js");

if($vconfig["use_qtip"] == 1){	//Determining if Qtip Tooltip should be used
	$document->addStyleSheet($compbase."css/jquery.qtip.min.css");
	$document->addScript($compbase."js/jquery.qtip.min.js");
}

	if(isset($_REQUEST["action"])){
		if($_REQUEST["action"] == "compareset"){
			if($_REQUEST["compare_value"] == 1){
				$_SESSION["vcompare"][$_REQUEST["compare_vid"]] = $_REQUEST["compare_vid"];
			}else{
				unset($_SESSION["vcompare"][$_REQUEST["compare_vid"]]);
			}
		}
	}
?>

<?php
	//echo JRequest::getInt('show_powerby')."<br />";
	if(JRequest::getInt('jquery_container') == 1)
	{
?>
	<div class="ui-widget ui-widget-content ui-corner-all" style="padding: 10px;">
<?php
	}else{
		echo "<div class=\"vlmcontent\">";	
	}
	
	if(count($_SESSION["vcompare"]) > 0)
	{
		
		$vcnt = count($_SESSION["vcompare"]);
?>

	<?php if($vcnt > 1){ ?>
    <p>You are comparing <?=$vcnt?> vehicles.</p>
    <?php }else{ ?>
    <p>You should select more vehicles to compare.</p>
    <?php } ?>
	
    <?php
		function tablerow($title,$row,$fieldname,$dobold = false){
			$outstr = "";
			$outstr .= "\n<tr>";
			$outstr .= "\n\t<th class=\"thighlight\">$title</th>";
				
			for($r=0; $r < count($row); $r++){
				$outstr .= "\n\t<td>";
				if($dobold){$outstr .= "<br>";}
				$outstr .= $row[$r][$fieldname];
				if($dobold){$outstr .= "</br>";}
				$outstr .= "</td>";	
			}
			$outstr .= "\n</tr>";
			return $outstr;
		}
	
		//echo implode(",",$_SESSION["vcompare"])."<br />";
		
		$vlistquery = "SELECT i.*, m.list_value as vmake_name, v.list_value as vmodel_name
		, (SELECT t.list_value FROM #__vlm_droplistvalues t WHERE prim_key = i.vtype) as vehicle_type
FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
ON i.vmake = m.prim_key
WHERE published = 1 AND vehicle_id IN (".$db->escape(implode(",",$_SESSION["vcompare"])).")
ORDER BY featured DESC, date_created";
		
		$db->setQuery($vlistquery);
		$row = $db->loadAssocList();
		
	?>
	
    <table id="tcompare">
    	<tr>
        	<th class="thighlight">Remove Compare</th>
            <?php
				for($r=0; $r < count($row); $r++){
					echo "<td>";
                    echo '<a href="javascript: setcompare('.$row[$r]["vehicle_id"].', 0);" title="Remove this vehicle from the campare list">
                    	<span><img src="'.$compbase.'images/remove.png" alt="Watching" align="absmiddle" />Remove</span>
                        </a>';
					echo "</td>";	
				}
			?>
        </tr>
        <tr>
        	<th class="thighlight">Title</th>
            <?php						
				for($r=0; $r < count($row); $r++){
					$mainimg = explode(";",$row[$r]["vimage"]);
					if($mainimg[0]==""){$titletip = "There is no image to preview";}
					else{$titletip = "<img src='".JURI::base().$mainimg[0]."' style='width:250px; height:auto' />";}
					echo "<td><a href=\"index.php/".$vconfig["main_url_segment"]."?option=com_vehiclelotmanager&vview=singlevehicle&id=".$row[$r]["vehicle_id"]."\" title=\"$titletip\">".$row[$r]["vtitle"]."</a></td>";	
				}
			?>
        </tr> 
        <?=tablerow("Vin #",$row,"vin_no")?> 
    	<?=tablerow("Asset Code",$row,"asset_code")?>
        <?=tablerow("Make",$row,"vmake_name")?>
        <?=tablerow("Model",$row,"vmodel_name")?>
    	<tr>
        	<th class="thighlight">Price</th>
            <?php
				for($r=0; $r < count($row); $r++){
					echo "<td>".$row[$r]["price_unit"]."".$vconfig["money_sign"]." ".$row[$r]["price"]."</td>";	
				}
			?>
        </tr>
        <?=tablerow("Type",$row,"vehicle_type")?>
        <?php if($vconfig["show_consession"]){ ?>
        <tr>
        	<th class="thighlight">Consession Price</th>
            <?php
				for($r=0; $r < count($row); $r++){
					echo "<td>";
					if($row[$r]["consession_price"] > 0){
						echo $row[$r]["price_unit"]."".$vconfig["money_sign"]." ".$row[$r]["consession_price"];
					}
					echo "</td>";
				}
			?>
        </tr> 
        <?php } ?>
    	<tr>
        	<th class="thighlight">Mileage</th>
            <?php
				for($r=0; $r < count($row); $r++){
					echo "<td>".$row[$r]["mileage_unit"]." ".$row[$r]["mileage"]."</td>";	
				}
			?>
        </tr>
        <?=tablerow("Year",$row,"vyear")?>
        <?=tablerow("Condition",$row,"vcondition")?>
        <?=tablerow("Transmission",$row,"transmission")?>
        <?=tablerow("Exterior Colour",$row,"colour_ext")?>
        <?=tablerow("Interior Colour",$row,"colour_int")?>
        <?=tablerow("Doors",$row,"doors")?>
        <?=tablerow("Engine",$row,"engine")?>
        <?=tablerow("Fuel Type",$row,"fuel_type")?>
        <?=tablerow("MPG",$row,"mpg")?>
        <tr>
        	<th class="thighlight">Features</th>
            <?php
				for($r=0; $r < count($row); $r++){
					$features = explode(";",$row[$r]["features"]);
					echo "<td valign=\"top\">";
					for($f = 0; $f < count($features); $f++){
						echo "<img src=\"".$compbase."images/".$vconfig["feature_bullet"]."\" alt='-' align='absmiddle' /> ".$features[$f]."<br />";
					}
					//echo "<td>".."".$vconfig["money_sign"]." ".$row[$r]["price"].
					echo "</td>";	
				}
			?>
        </tr>
    </table>
<br />
	<form method="post" id="comparedata">
    	<input type="hidden" name="compare_vid" id="compare_vid" />
        <input type="hidden" name="compare_value" id="compare_value" />
        <input type="hidden" name="action" value="compareset" />
    </form>
    <script>
		function setcompare(vid, cvalue){
			$("#compare_vid").val(vid);
			$("#compare_value").val(cvalue);
			
			document.forms["comparedata"].submit();
		}
	</script>
    
<?php }else{ ?>
	<p>You have not selected any vehicles to compare.</p>
<?php }?>


	</div>
<?php
	//echo JRequest::getInt('show_powerby')."<br />";
	if(JRequest::getInt('show_powerby') == 1)
	{
?>
	<?php include("vview/poweredby.php"); ?>
<?php
	}else{
		echo "Nothing";	
	}
?>