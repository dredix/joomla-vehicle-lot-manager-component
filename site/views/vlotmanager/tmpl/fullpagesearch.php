<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$dconfig["comp_name"] = "com_vehiclelotmanager";
$dconfig["baseurl"] = JURI::base();

$compbase = $dconfig["baseurl"]."components/".$dconfig["comp_name"]."/";
$acompbase = $dconfig["baseurl"]."administrator/components/".$dconfig["comp_name"]."/";

if (!function_exists("getdbconfig")){
	function getdbconfig()
	{
		$db =& JFactory::getDBO();
		$db->setQuery("SELECT soption, svalue FROM #__vlm_config");
		$row = $db->loadAssocList();
		
		for($r = 0; $r < count($row); $r++){
			$outarr[$row[$r]["soption"]] = $row[$r]["svalue"];
		}
		return $outarr;
	}
}

if (!function_exists("getdroplist")){
	function getdroplist($listcat, $selectval ="", $prim_as_select = true){
		//$listcat			=	string		Category to get data from		
		//$selectval		=	string		Value if found to be selected in the drop list
		//$prim_as_select	=	boolean		If true, the prim_key will be used as the option value, else, the display name
		$db =& JFactory::getDBO();
		$query = "SELECT prim_key, list_value FROM #__vlm_droplistvalues WHERE list_category = '$listcat' ORDER BY ordering, list_value";
		$db->setQuery($query);
		$row = $db->loadAssocList();
		$ret_list = "";
		for($r=0; $r < count($row); $r++){
			if($prim_as_select){ 
				//Using display name as value
				if($row[$r]["prim_key"] == $selectval){$sel = "selected=\"selected\"";}else{$sel = "";};
				$ret_list .= "\t<option value=\"".$row[$r]["prim_key"]."\" $sel>".$row[$r]["list_value"]."</option>\n";
			}else{ 
				//Using primary key as value
				if($row[$r]["list_value"] == $selectval){$sel = "selected=\"selected\"";}else{$sel = "";};
				$ret_list .= "\t<option value=\"".$row[$r]["list_value"]."\" $sel>".$row[$r]["list_value"]."</option>\n";
			}
		}
		return $ret_list;
	}
}
	
$vconfig = getdbconfig();
$dconfig["jtheme"] = $vconfig["front_jquery_theme"];

$db =& JFactory::getDBO();						//Used to database handling		http://docs.joomla.org/Accessing_the_database_using_JDatabase
$document =& JFactory::getDocument();


$document->addStyleSheet($acompbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
$document->addScript($acompbase."js/jquery-1.7.1.js");
$document->addScript($acompbase."js/jquery.ui.core.js");
$document->addScript($acompbase."js/jquery.ui.widget.js");
$document->addScript($acompbase."js/jquery.ui.accordion.js");

if($vconfig["use_qtip"] == 1){	//Determining if Qtip Tooltip should be used
	$document->addStyleSheet($compbase."css/jquery.qtip.min.css");
	$document->addScript($compbase."js/jquery.qtip.min.js");
}

?>
<style>
#vtsearch{
	width: 100%;
}
#vtsearch tr td{
	width: 33%;
	vertical-align:top;
	padding:20px;
}
#vtsearch .i_med, div.vsoption .i_med{
	width: 150px;
	background: none repeat scroll 0 0 #F7F7F7;
    border: 1px solid #D8D8D8;
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 2px 2px rgba(0, 0, 0, 0.07) inset;
    margin-bottom: 4px;
    margin-top: 4px;
    padding: 4px;
}
#vtsearch .i_small, div.vsoption .i_small{
	width: 75px;
	background: none repeat scroll 0 0 #F7F7F7;
    border: 1px solid #D8D8D8;
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 2px 2px rgba(0, 0, 0, 0.07) inset;
    margin-bottom: 4px;
    margin-top: 4px;
    padding: 4px;
}

#vtsearch label, , div.vsoption label{
	font-weight: bold;
}

div.vsoption
{
	padding: 5px 20px;
	margin:0px 20px;
	float: left;
}
</style>

	<script>
	$(function() {
		$( ".vaccordion" ).accordion({
			collapsible: true
		});
	});
	</script>
    
<div class="vaccordion" id="searchoption">
	<h3><a href="#">Search Options</a></h3>
	<div>    
        <form method="post" action="index.php/<?=$vconfig["search_url_segment"] ?>?option=com_vehiclelotmanager&view=vlotmanager&layout=fullpagesearch#searchresults">
            <input type="hidden" value="search" name="dosearch" />
            <div class="vsoption">
                <label>Make</label><br />
                <select name="vmake" class="i_med">
                    <option value=""></option>
                    <?=getdroplist("Make",$_REQUEST["vmake"])?>
                </select>
            </div>
            
            <div class="vsoption">
                <label>Model</label><br />
                <select name="vmodel" class="i_med">
                    <option value=""></option>
                    <?=getdroplist("Model",$_REQUEST["vmodel"])?>
                </select>
            </div>
            
            <div class="vsoption">
                <label>Type</label><br />
                <select name="vtype" class="i_med">
                    <option value=""></option>
                    <?=getdroplist("Type",$_REQUEST["vtype"])?>
                </select>
            </div>
            
            <div class="vsoption">
                <label>Transmission</label><br />
                <select name="transmission" class="i_med">
                    <option value=""></option>
                    <?=getdroplist("Transmission",$_REQUEST["transmission"],false)?>
                </select>
            </div>
            
            <div class="vsoption">
                <label>Doors</label><br />
                <select name="doors" class="i_med">
                    <option value=""></option>
                    <?=getdroplist("Doors",$_REQUEST["doors"],false)?>
                </select>
            </div>
            
            <div class="vsoption">
                <label>Fuel Type</label><br />
                <select name="fuel_type" class="i_med">
                    <option value=""></option>
                    <?=getdroplist("Fuel Type",$_REQUEST["fuel_type"],false)?>
                </select>
            </div>
            
            <div class="vsoption">
                <label>Price</label><br />
                <input name="start_price" class="i_small" value="<?=$_REQUEST["start_price"]?>" /> to <input name="end_price" class="i_small" value="<?=$_REQUEST["end_price"]?>" />
            </div>
            
            <div class="vsoption">
                <label>Year</label><br />
                <?php
					$yearsql = "SELECT min(vyear) as minyear, max(vyear) as maxyear FROM #__vlm_vehicle h;";
					$db->setQuery($yearsql);
					$yrow = $db->loadRow();
				?>
				
                <select name="start_year" class="i_small">
                	<option value=""></option>
                    <?php
						for($y = $yrow[0]; $y <= $yrow[1]; $y++){
							if($_REQUEST["start_year"] == $y) {$ysel = 'selected="selected"';}else{$ysel ="";}
							echo "\n\t\t<option value=\"$y\" $ysel >$y</option>";
						}
					?>
                </select>
                 to 
                <select name="end_year" class="i_small">
                	<option value=""></option>
                    <?php
						for($y = $yrow[0]; $y <= $yrow[1]; $y++){
							if($_REQUEST["end_year"] == $y) {$ysel = 'selected="selected"';}else{$ysel ="";}
							echo "\n\t\t<option value=\"$y\" $ysel >$y</option>";
						}
					?>
                </select> 
                 <br />
            </div>   
            
            <div class="clr"></div>
            <input type="submit" value="Search"  /> <input type="reset" value="Clear Search Options" />
        </form>
	</div>
</div>
<br />
<script>
	//$( "#searchoption" ).hide()
	//$( "#searchoption" )..accordion( "activate" , 0 )	;
</script>


    
<?php if(isset($_REQUEST["dosearch"])){ ?>
<a name="searchresults"></a>
<div class="vaccordion">
	<h3><a href="#">Search Results</a></h3>
	<div>
		<?php 
		$sqlfilter  = "";
		if($_REQUEST["vmake"] != ""){ $sqlfilter .= " AND vmake =".$db->Escape($_REQUEST["vmake"]); }
		if($_REQUEST["vmodel"] != ""){ $sqlfilter .= " AND vmodel =".$db->Escape($_REQUEST["vmodel"]); }
		if($_REQUEST["vtype"] != ""){ $sqlfilter .= " AND vtype =".$db->Escape($_REQUEST["vtype"]); }
		if($_REQUEST["transmission"] != ""){ $sqlfilter .= " AND transmission =".$db->Quote($db->Escape($_REQUEST["transmission"])); }
		if($_REQUEST["doors"] != ""){ $sqlfilter .= " AND doors =".$db->Quote($db->Escape($_REQUEST["doors"])); }
		if($_REQUEST["fuel_type"] != ""){ $sqlfilter .= " AND fuel_type =".$db->Quote($db->Escape($_REQUEST["fuel_type"])); }
		if(($_REQUEST["start_price"] != "") && ($_REQUEST["end_price"] != "")){ $sqlfilter .= " AND price >=".$db->Quote($db->Escape($_REQUEST["start_price"]))." AND price <=".$db->Quote($db->Escape($_REQUEST["end_price"])); }
		if(($_REQUEST["start_year"] != "") && ($_REQUEST["end_year"] != "")){ $sqlfilter .= " AND vyear >=".$db->Quote($db->Escape($_REQUEST["start_year"]))." AND vyear <=".$db->Quote($db->Escape($_REQUEST["end_year"])); }
		
		$vlistquery = "SELECT i.*, m.list_value as vmake_name, v.list_value as vmodel_name
		, (SELECT t.list_value FROM #__vlm_droplistvalues t WHERE prim_key = i.vtype) as vehicle_type
	FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
	ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
	ON i.vmake = m.prim_key
	WHERE published = 1  $sqlfilter
	ORDER BY featured DESC, date_created DESC";	
		
		$showcount = 1;
		include("vview/vlist.php"); 
		?>
        
	</div>
</div>
    


<?php } ?>

<?php include("vview/poweredby.php"); ?>