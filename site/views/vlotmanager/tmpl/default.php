<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$dconfig["comp_name"] = "com_vehiclelotmanager";
$dconfig["baseurl"] = JURI::base();

$compbase = $dconfig["baseurl"]."components/".$dconfig["comp_name"]."/";
$acompbase = $dconfig["baseurl"]."administrator/components/".$dconfig["comp_name"]."/";

if (!function_exists("getdbconfig")){
	function getdbconfig()
	{
		$db =& JFactory::getDBO();
		$db->setQuery("SELECT soption, svalue FROM #__vlm_config");
		$row = $db->loadAssocList();
		
		for($r = 0; $r < count($row); $r++){
			$outarr[$row[$r]["soption"]] = $row[$r]["svalue"];
		}
		return $outarr;
	}
}
$vconfig = getdbconfig();
$dconfig["jtheme"] = $vconfig["front_jquery_theme"];

$db =& JFactory::getDBO();						//Used to database handling		http://docs.joomla.org/Accessing_the_database_using_JDatabase
$document =& JFactory::getDocument();


$document->addStyleSheet($acompbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");
if($vconfig["use_comp_jquery"] == 1){ $document->addScript($acompbase."js/jquery-1.7.1.js");}
$document->addScript($acompbase."js/jquery.ui.core.js");
$document->addScript($acompbase."js/jquery.ui.widget.js");
$document->addScript($acompbase."js/jquery.ui.accordion.js");

if($vconfig["use_qtip"] == 1){	//Determining if Qtip Tooltip should be used
	$document->addStyleSheet($compbase."css/jquery.qtip.min.css");
	$document->addScript($compbase."js/jquery.qtip.min.js");
}

$showcount = 0;
?>
	<script>
	$(function() {
		$( ".vaccordion" ).accordion({
			collapsible: true
		});
	});
	</script>

<?php

//Allow loading of module position in component
$modules =& JModuleHelper::getModules('slider_area');
foreach ($modules as $module){
   echo JModuleHelper::renderModule($module);
}

?>


<? if(JRequest::getVar('header_html') != "") { ?>

<div class="vaccordion">
	<h3><a href="#"> </a></h3>
	<div>
		<?=JRequest::getVar('header_html');?>
	</div>
</div>
<br />
<? } ?>

<?php
	if(JRequest::getInt('show_featured') == 1)
	{
?>
<div class="vaccordion">
	<h3><a href="#">Featured Vehicle</a></h3>
	<div>
		<?php 
		$vlistquery = "SELECT i.*, m.list_value as vmake_name, v.list_value as vmodel_name
		, (SELECT t.list_value FROM #__vlm_droplistvalues t WHERE prim_key = i.vtype) as vehicle_type
	FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
	ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
	ON i.vmake = m.prim_key
	WHERE published = 1 AND featured = 1
	ORDER BY featured DESC, date_created DESC";	
		include("vview/vlist.php"); 
		?>
	</div>
</div>
<br />
<?php
	}
	if(JRequest::getInt('show_newest') == 1)
	{
?>
<div class="vaccordion">
	<h3><a href="#">Newest Vehicle</a></h3>
	<div>
		<?php 
		$vlistquery = "SELECT i.*, m.list_value as vmake_name, v.list_value as vmodel_name
		, (SELECT t.list_value FROM #__vlm_droplistvalues t WHERE prim_key = i.vtype) as vehicle_type
	FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
	ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
	ON i.vmake = m.prim_key
	WHERE published = 1
	ORDER BY date_created DESC
	LIMIT 10";	
		include("vview/vlist.php"); 
		?>
	</div>
</div>
<?php
	}
?>

<? if(JRequest::getVar('footer_html') != "") { ?>
<br />
<div class="vaccordion">
	<h3><a href="#"> </a></h3>
	<div>
		<?=JRequest::getVar('footer_html');?>
	</div>
</div>
<? } ?>


<?php include("vview/poweredby.php"); ?>