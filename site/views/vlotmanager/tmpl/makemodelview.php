<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$dconfig["comp_name"] = "com_vehiclelotmanager";
$dconfig["baseurl"] = JURI::base();

$compbase = $dconfig["baseurl"]."components/".$dconfig["comp_name"]."/";
$acompbase = $dconfig["baseurl"]."administrator/components/".$dconfig["comp_name"]."/";

if (!function_exists("getdbconfig")){
	function getdbconfig()
	{
		$db =& JFactory::getDBO();
		$db->setQuery("SELECT soption, svalue FROM #__vlm_config");
		$row = $db->loadAssocList();
		
		for($r = 0; $r < count($row); $r++){
			$outarr[$row[$r]["soption"]] = $row[$r]["svalue"];
		}
		return $outarr;
	}
}
$vconfig = getdbconfig();
$dconfig["jtheme"] = $vconfig["front_jquery_theme"];

$db =& JFactory::getDBO();						//Used to database handling		http://docs.joomla.org/Accessing_the_database_using_JDatabase
$document =& JFactory::getDocument();


$document->addStyleSheet($acompbase."css/".$dconfig["jtheme"]."jquery.ui.all.css");

if($vconfig["use_comp_jquery"] == 1){ $document->addScript($acompbase."js/jquery-1.7.1.js");}

if($vconfig["use_qtip"] == 1){	//Determining if Qtip Tooltip should be used
	$document->addStyleSheet($compbase."css/jquery.qtip.min.css");
	$document->addScript($compbase."js/jquery.qtip.min.js");
}

?>

<?php
	//echo JRequest::getInt('show_powerby')."<br />";
	if($vconfig["use_jquery_container"] == 1)
	{
?>
	<div class="ui-widget ui-widget-content ui-corner-all" style="padding: 10px;">
<?php
	}else{
		echo "<div class=\"vlmcontent\">";	
	}
	
	
	//Checking which view the user is at
	
	if(isset($_REQUEST["vview"]))
	{
		if($_REQUEST["vview"]=="singlemake")
		{
			//A make has been selected 
			$vlistquery = "SELECT i.*, m.list_value as vmake_name, v.list_value as vmodel_name
			, (SELECT t.list_value FROM #__vlm_droplistvalues t WHERE prim_key = i.vtype) as vehicle_type
FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
ON i.vmake = m.prim_key
WHERE published = 1 AND vmake = ".$db->Escape($_REQUEST["makeid"])."
ORDER BY featured DESC, date_created";
			
			$db->setQuery("SELECT list_value FROM #__vlm_droplistvalues WHERE prim_key = ".$db->Escape($_REQUEST["makeid"]));
			$ptitle = $db->loadResult();
?>
	<h3><?=$ptitle?></h3>	
    <!--    
	<div id="wrapper">
		<div class="accordionButton">Button 1 Content</div>
		<div class="accordionContent">Content 1<br /><br /><br /><br /><br /><br /><br /><br />Long Example</div>
		<div class="accordionButton">Button 2 Content</div>
		<div class="accordionContent">Content 2<br /><br /><br /><br /><br />Medium Example</div>
		<div class="accordionButton">Button 3 Content</div>
		<div class="accordionContent">Content 1<br />Short Example</div>
		<div class="accordionButton">Button 4 Content</div>
		<div class="accordionContent">Content 4<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />Extra Long Example</div>
	</div>
    -->
       
	<? include("vview/vlist.php"); 
	
		}
		elseif($_REQUEST["vview"]=="singlemodel")
		{
			//A make has been selected 
			$vlistquery = "SELECT i.*, m.list_value as vmake_name, v.list_value as vmodel_name
			, (SELECT t.list_value FROM #__vlm_droplistvalues t WHERE prim_key = i.vtype) as vehicle_type
FROM #__vlm_vehicle i LEFT JOIN #__vlm_droplistvalues v
ON i.vmodel = v.prim_key LEFT JOIN #__vlm_droplistvalues m
ON i.vmake = m.prim_key
WHERE published = 1 AND vmodel = ".$db->Escape($_REQUEST["modelid"]);
			$db->setQuery("SELECT list_value FROM #__vlm_droplistvalues WHERE prim_key = ".$db->Escape($_REQUEST["modelid"]));
			$ptitle = $db->loadResult();
?>
		<h3><?=$ptitle?></h3>
        	
		<? include("vview/vlist.php"); 
		
		
		}
		elseif($_REQUEST["vview"]=="singlevehicle")
		{
			
			//A make has been selected 
			 include("vview/vsinglevehicle.php"); 
		
		}
	}else{
		//Checking which view the user is at
		//No view has been selected
		
		include("vview/accordionlist.php");

	}
?>

	</div>

<?php
	//echo JRequest::getInt('show_powerby')."<br />";
	if(JRequest::getInt('show_powerby') == 1)
	{
?>
	<?php include("vview/poweredby.php"); ?>
<?php
	}else{
		echo "Nothing";	
	}
?>